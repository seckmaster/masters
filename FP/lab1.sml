(*  Vrne fakulteto števila n, n >= 0. *)
fun factorial (n: int): int =
	if n = 0 then 1
	else n * factorial(n - 1);

factorial 0;
factorial 1;
factorial 2;
factorial 3;
factorial 4;
factorial 5;
factorial 6;

(*  Vrne n-to potenco števila x, n >= 0. *)
fun power (x: int, n: int): int =
	if n = 0 then 1
	else x * power(x, n - 1);

power(10, 0);
power(10, 1);
power(10, 5);
power(2, 1);
power(2, 2);
power(2, 24);

(*  Vrne največjega skupnega delitelja
    pozitivnih števil a in b, a >= b. *)
fun gcd (a: int, b: int): int =
	if b = 0 then a
	else gcd(b, a mod b);

gcd(9, 6);
gcd(100, 100);
gcd(25, 100);
gcd(27126, 2312);

(*  Vrne dolžino seznama. *)
fun len (xs: int list): int =
	if null xs then 0
	else 1 + len(tl xs);

len([1, 2, 3]);
len([]);

(*  Vrne SOME zadnji element seznama.
    Če je seznam prazen vrne NONE. *)
fun last (xs: int list): int option =
	if null xs then NONE
	else if null (tl xs) then SOME (hd xs)
	else last(tl xs);

last [];
last (1::[2, 3, 4]);
last (1::2::3::4::[]);
last [1, 2, 3, 4];

(*  Vrne SOME n-ti element seznama.
    Prvi element ima indeks 0.
    Če indeks ni veljaven, vrne NONE. *)
fun nth (xs: int list, n: int): int option = 
	if null xs then NONE
	else if n = 0 then SOME (hd xs)
	else nth(tl xs, n - 1);

isSome (nth([], 0));
nth([], 2);
nth([1, 2, 3], 0);
nth([1, 2, 3], 1);
nth([1, 2, 3], 2);
nth([1, 2, 3], 3);
nth([1, 2, 3, 4, 5, 6], 5);

(*  Vrne nov seznam, ki je tak kot vhodni,
    le da je na n-to mesto vrinjen element x.
    Prvo mesto v seznamu ima indeks 0.
    Indeks n je veljaven (0 <= n <= length xs). *)
fun insert (xs: int list, n: int, x: int): int list =
	if null xs then x::[]
	else if n = 0 then x::xs
	else (hd xs)::insert(tl xs, n - 1, x);

insert([], 0, 10);
insert([], 1, 10);
insert([], 100, 10);
insert([1, 2, 4], 2, 3);
insert([1, 2, 3, 4], 4, 5);
insert([2, 3, 4, 4], 0, 1);
insert([1, 3, 4, 5], 1, 2);
insert([1, 2, 4, 5], 2, 3);

(*  Vrne nov seznam, ki je tak kot vhodni,
    le da so vse pojavitve elementa x odstranjene. *)
fun delete (xs: int list, x: int): int list =
	if null xs then []
	else if hd xs = x then delete(tl xs, x)
	else (hd xs)::delete(tl xs, x);

delete([], 10);
delete([1, 2, 5, 1, 3, 1, 10, 5, 1], 1);
delete([10, 10, 10, 10], 10);
delete([1, 2, 4, 5, 6], 55);

fun append_int(xs: int list, x: int) =
    insert(xs, len(xs), x);

(*  Vrne nov seznam, ki je tak kot vhodni,
    le da so vse pojavitve elementa x odstranjene. *)
fun delete (xs: int list, x: int): int list =  
    if null xs then []
    else if (hd xs) = x then delete(tl xs, x)
    else (hd xs)::delete(tl xs, x);

(*  Vrne obrnjen seznam. V pomoč si lahko spišete
    še funkcijo append, ki doda na konec seznama. *)
fun reverse (xs: int list): int list =
    if null xs then nil
    else insert(reverse(tl xs), len(xs), hd xs);

reverse [];
reverse [1, 2, 3, 4, 5];

(*  Vrne true, če je podani seznam palindrom.
    Tudi prazen seznam je palindrom. *)
fun palindrome (xs: int list): bool =
    xs = reverse(xs); 

palindrome [];
palindrome [1, 1, 2, 1, 1];
palindrome [1, 2, 3, 4, 5];
palindrome [1, 2, 3, 3, 2, 1];
palindrome [1, 1, 1, 1, 1];
palindrome [1, 2, 1, 1, 1];
palindrome [10, 1, 2, 1, 1];
