(* Podan seznam xs agregira z začetno vrednostjo z
   in funkcijo f v vrednost f(f(f(z, xs_1),  xs_2), xs_3) ... *)
fun reduce (f: 'a * 'b -> 'a, z: 'a, xs: 'b list): 'a =
   if null xs then z
   else reduce (f, f (z, hd xs), tl xs);

reduce ((fn (acc, x) => acc + x), 0, [1, 2, 3, 4]) = 10;
reduce ((fn (acc, x) => acc + x), 5, [1, 2, 3, 4]) = 15;
reduce ((fn (acc, x) => acc + x), 0, []) = 0;

(* Vrne seznam, ki vsebuje kvadrate števil
   iz vhodnega seznama. Uporabite List.map. *)
fun squares (xs: int list): int list = List.map (fn (a) => a * a) xs;

squares ([2, 3, 4]) = [4, 9, 16];

(* Vrne seznam, ki vsebuje vsa soda števila
   iz vhodnega seznama. Uporabite List.filter. *)
fun onlyEven (xs: int list): int list = List.filter (fn (a) => a mod 2 = 0) xs;

onlyEven ([1, 2, 3, 4, 5, 6, 7]) = [2, 4, 6];

(* Vrne najboljši niz glede na funkcijo f. Funkcija f primerja dva niza
   in vrne true, če je prvi niz boljši od drugega. Uporabite List.foldl.
   Najboljši niz v praznem seznamu je prazen niz. *)
fun bestString (f: string * string -> bool, xs: string list): string =
   List.foldl (fn (a, b) => if f (a, b) then a else b) "" xs;

bestString ((fn (a, b) => (String.size a) > (String.size b)), ["abc", "aa", "efgh", "x"]) = "efgh";

(* Vrne leksikografsko največji niz. Uporabite bestString. *)
fun largestString (xs: string list): string = bestString (String.>, xs);

largestString (["abc", "aa", "efgh", "x"]) = "x";   

(* Vrne najdaljši niz. Uporabite bestString. *)
fun longestString (xs: string list): string =
   bestString ((fn (a, b) => (String.size a) > (String.size b)), xs);

longestString (["abc", "aa", "efgh", "x"]) = "efgh";

(* Seznam uredi naraščajoče z algoritmom quicksort. *)
fun quicksort (xs: int list): int list = 
   if null xs then []
   else let 
      fun partition (xs, x) = List.partition (fn (a) => a < x) xs         
      val (l, r) = partition (tl xs, hd xs)
   in
      quicksort (l) @ [hd xs] @ quicksort (r)
   end;

quicksort ([3, 6, 1, 8, 10, 12, 9]) = [1, 3, 6, 8, 9, 10, 12];
quicksort ([1, 3, 6, 8, 9, 10, 12]) = [1, 3, 6, 8, 9, 10, 12];
quicksort ([12, 10, 9, 8, 6, 3, 1]) = [1, 3, 6, 8, 9, 10, 12];

(* Vrne skalarni produkt dveh vektorjev.
   Uporabite List.foldl in ListPair.map. *)
fun dot (xs: int list, ys: int list): int =
   List.foldl (fn (x, y) => x + y) 0 (ListPair.map (fn (a, b) => a * b) (xs, ys));

dot ([3, 4, 5], [1, 2, 3]) = 26;

(* Vrne transponirano matriko. Matrika je podana z vrstičnimi vektorji
   od zgoraj navzdol: [[1,2,3],[4,5,6],[7,8,9]] predstavlja matriko
   [ 1 2 3 ]
   [ 4 5 6 ]
   [ 7 8 9 ]
   *)
fun transpose (m: 'a list list): 'a list list =
   let 
      fun transpose_ (m: 'a list list) =
         if null m then []
         else hd (hd m)::transpose_ (tl m)

      fun remove_heads (m: 'a list list): 'a list list =
         if null m then []
         else tl (hd m)::remove_heads (tl m)
   in
      if null (hd m) then []
      else transpose_ (m)::transpose (remove_heads (m))
   end;

val A = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
val B = [[1, 4, 7], [2, 5, 8], [3, 6, 9]];

transpose (A) = B;

(* Zmnoži dve matriki. Uporabite dot in transpose. *)
fun multiply (a: int list list, b: int list list): int list list =
   let 
      fun mul (a, b) = 
         if null b then []
         else dot (a, hd b)::mul (a, tl b)

      fun multiply (a, b) = 
         if null a then []
         else mul (hd a, b)::multiply (tl a, b)
   in
      multiply (a, transpose b)
   end;

val C = [[14,32,50],[32,77,122],[50,122,194]];
multiply (A, B) = C;

(* V podanem seznamu prešteje zaporedne enake elemente in vrne seznam
   parov (vrednost, število ponovitev). Podobno deluje UNIX-ovo orodje
   uniq -c. *)
fun group (xs: ''a list): (''a * int) list =
   let 
      fun group(xs, res, count) =
         if null xs then res
         else if null (tl xs) then res @ [(hd xs, count + 1)]
         else if (hd xs) = (hd (tl xs)) then group (tl xs, res, count + 1)
         else group (tl xs, res @ [(hd xs, count + 1)], 0)
   in
      group (xs, [], 0)
   end;

group ([1, 1, 1, 2, 2, 3, 4, 3, 3]) = [(1,3),(2,2),(3,1),(4,1),(3,2)];

(* Elemente iz podanega seznama razvrsti v ekvivalenčne razrede.
   Znotraj razredov naj bodo elementi v istem vrstnem redu kot v
   podanem seznamu. Ekvivalentnost elementov definira funkcija f,
   ki za dva elementa vrne true, če sta ekvivalentna. *)
fun equivalenceClasses (f: ''a * ''a -> bool, xs: ''a list): ''a list list =
   let 
      (*fun filter (xs, a) = List.filter (fn (x) => f (a, x)) xs
      val _ = print (Int.toString (hd xs))*)
      fun filter (xs, a) =
         if null xs then []
         else if f (a, hd xs) then (hd xs)::filter (tl xs, a)
         else filter (tl xs, a)

      fun equivalenceClasses (xss) =
         if null xss then []
         else filter (xs, hd xss)::equivalenceClasses (tl xss)
   in
      equivalenceClasses (xs)
   end;

equivalenceClasses ((fn (a, b) => (a + b) = 10), [1, 2, 3, 4, 5, 6, 7]) = 
   [[],[],[7],[6],[5],[4],[3]];
equivalenceClasses ((fn (a, b) => a = 2), [1, 2, 3, 4, 5, 6, 7]) =  
   [[],[1, 2, 3, 4, 5, 6 ,7],[],[],[],[],[]];
