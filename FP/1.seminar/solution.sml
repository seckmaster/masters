use "cookbook-sl.sml";
exception NotImplemented

(************* Utils *************)

(* Sort list of elements using `f` for comparing. *)
fun sort [] f = []
    | sort (hd::tl) f = 
    let 
        fun partition (xs, x) = List.partition (fn (a) => f (a, x)) xs
        val (l, r) = partition (tl, hd)
    in
      (sort l f) @ [hd] @ (sort r f)
   end;

(* Find minimum element in `xs` by comparing elements using `f`. *)
fun min xs f =
    let 
        fun min [] current = current
            | min (hd::tl) NONE = min tl (SOME hd)
            | min (hd::tl) (SOME currentMin) = if f (hd, currentMin) then min tl (SOME hd) else min tl (SOME currentMin)
    in
        min xs NONE
    end;

(* Return `true` if `el` is in the given list, otherwise `false` *)
fun contains [] el f = false
    | contains (hd::tl) el f = if f (hd, el) then true else contains tl el f;

(* Remove `NONE`s from the given list. I.e. [SOME 1, NONE, SOME 2, NONE, NONE, SOME 3] => [1,2,3] *)
fun compactMap xs =
    let
        fun compactMap [] acc = acc
            | compactMap ((SOME hd)::tl) acc = compactMap tl (acc @ [hd])
            | compactMap (NONE::tl) acc = compactMap tl acc
    in
        compactMap xs []
    end;

(************* End utils *************) 

structure Dictionary :> DICTIONARY =
struct
    type (''key, 'value) dict = (''key * 'value) list

    val empty = []

    fun exists dict key =
        case dict of 
            [] => false
            | (curr_key, _)::tl => curr_key = key orelse exists tl key;

    fun isEmpty [] = true | isEmpty _ = false;

    fun size dict =
        let 
            fun size (_::tl) acc = size tl acc + 1
                | size [] acc = acc
        in
            size dict 0
        end;
    
    fun get [] key = NONE
        | get ((curr_key, value)::tl) key = if curr_key = key then SOME value 
                                            else get tl key;

    fun getOrDefault dict key default = 
        let 
            val value = get dict key 
        in
            case value of 
                SOME x => x
                | NONE => default
        end;
    
    fun set dict key value =
        let
            fun insert_or_update [] = (key, value)::[]
                | insert_or_update ((curr_key, curr_val)::tl) = if curr_key = key then (key, value)::tl
                                                                else (curr_key, curr_val)::insert_or_update tl;
        in
            insert_or_update dict
        end;

    fun remove [] key = empty
        | remove ((curr_key, curr_val)::tl) key = if curr_key = key then tl 
                                                  else (curr_key, curr_val)::remove tl key;
    
    fun keys dict =
        let 
            fun keys [] acc = acc
                | keys ((key, _)::tl) acc = keys tl (acc @ [key])
        in
            keys dict []
        end;


    fun values dict = 
        let 
            fun values [] acc = acc
                | values ((_, value)::tl) acc = values tl (acc @ [value])
        in
            values dict []
        end;

    fun toList dict = 
        let 
            fun toList [] acc = acc
                | toList (el::tl) acc = toList tl (acc @ [el])
        in 
            toList dict []
        end;

    fun fromList list = 
        let 
            fun fromList [] dict = dict
                | fromList ((key, value)::tl) dict = fromList tl (set dict key value)
        in
            fromList list empty
        end;

    fun merge a [] = a
        | merge a ((key, value)::tl) = merge (set a key value) tl;
        

    fun filter f dict = 
        let 
            fun filter [] acc = acc
                | filter ((key, value)::tl) acc = if f (key, value) then filter tl (set acc key value)
                                                  else filter tl acc
        in
            filter dict empty
        end;

    fun map f dict = 
        let
            fun map [] acc = acc
                | map ((key, value)::tl) acc = 
                    let 
                        val (new_key, new_value) = f (key, value)
                    in
                        map tl (set acc new_key new_value)
                    end
        in
            map dict empty
        end;
end;

structure Cookbook :> COOKBOOK =
struct
    type ingredient = string
    type stock = (ingredient, int) Dictionary.dict
    type pricelist = (ingredient, real) Dictionary.dict
    type recipe = ingredient * stock
    type cookbook = (ingredient, stock) Dictionary.dict

    exception NoPriceException

    fun makeIngredient name = name;
    fun makeStock ingredients = Dictionary.fromList ingredients;
    fun makePricelist pricelist = Dictionary.fromList pricelist;
    fun makeRecipe recipe = recipe;
    fun makeCookbook recipes = Dictionary.fromList recipes;

    fun ingredientToString ingredient = ingredient;

    fun toString [] keyTransform valueTransform acc = acc
        | toString ((key, value)::[]) keyTransform valueTransform acc = acc ^ keyTransform key ^ ": " ^ valueTransform value
        | toString ((key, value)::tl) keyTransform valueTransform acc = toString tl keyTransform valueTransform (acc ^ keyTransform key ^ ": " ^ valueTransform value ^ "\n");

    fun stockToString stock = 
        let 
            fun sorted stock = sort stock (fn ((a_key, _), (b_key, _)) => 
                    (ingredientToString a_key) < (ingredientToString b_key))
        in
            toString (sorted (Dictionary.toList (Dictionary.filter (fn (_, value) => value > 0) stock))) ingredientToString Int.toString ""
        end;

    fun pricelistToString pricelist =
        let 
            fun sorted pricelist = sort pricelist (fn ((a_key, _), (b_key, _)) => 
                    (ingredientToString a_key) < (ingredientToString b_key))
        in
            toString (sorted (Dictionary.toList pricelist)) ingredientToString Real.toString ""
        end;

    fun recipeToString (ingredient, stock) = 
        "=== " ^ ingredientToString ingredient ^ " ===\n" ^ stockToString stock;

    fun cookbookToString cookbook = 
        let
            fun sorted recipes = sort recipes (fn ((a_key, _), (b_key, _)) => 
                (ingredientToString a_key) < (ingredientToString b_key))
            fun toString [] str = str
                | toString (recipe::[]) str = str ^ recipeToString recipe
                | toString (recipe::tl) str = toString tl (str ^ recipeToString recipe ^ "\n\n")
        in
            toString (sorted (Dictionary.toList cookbook)) ""
        end;

    fun hasEnoughIngredients stock (_, recipe) =
        let 
            fun hasEnoughIngredients stock [] = true
                | hasEnoughIngredients stock ((key, value)::tl) = 
                    let 
                        val inStock = Dictionary.getOrDefault stock key 0
                    in
                        inStock >= value andalso hasEnoughIngredients stock tl
                    end
        in
            hasEnoughIngredients stock (Dictionary.toList recipe)
        end;

    fun cook (name, recipe) stock = 
        let
            fun cook stock [] = Dictionary.set stock name 1
                | cook stock ((key, value)::tl) = 
                    let
                        val inStock = Dictionary.getOrDefault stock key 0
                        val remaining = inStock - value
                    in
                        cook (Dictionary.set stock key remaining) tl
                    end
        in
            cook stock (Dictionary.toList recipe)
        end

    fun priceOfStock stock pricelist = 
        let
            fun priceOfStock [] acc = acc
                | priceOfStock ((key, value)::tl) acc = 
                    case Dictionary.get pricelist key of 
                        NONE => raise NoPriceException
                        | SOME price => priceOfStock tl (acc + Real.fromInt (value) * price)
        in
            priceOfStock (Dictionary.toList stock) 0.0
        end;

    fun priceOfRecipe (_, recipe) pricelist = priceOfStock recipe pricelist;

    (*fun missingIngredients (_, recipe) stock = 
        let 
            fun missingIngredients [] new_stock = new_stock
                | missingIngredients ((key, value)::tl) new_stock = 
                    case Dictionary.get stock key of 
                        NONE => missingIngredients tl (Dictionary.set new_stock key value)
                        | SOME _ => missingIngredients tl new_stock
        in
            missingIngredients (Dictionary.toList recipe) Dictionary.empty
        end;*)

    fun missingIngredients recipe stock = 
        Dictionary.map (fn (key, value) => (key, value * ~1)) (Dictionary.filter (fn (_, value) => value < 0) (cook recipe stock));

    (*fun possibleRecipes cookbook stock = 
        let
            fun possibleRecipes [] new_cookbook = new_cookbook
                | possibleRecipes ((key, value)::tl) new_cookbook = if hasEnoughIngredients stock (key, value) then possibleRecipes tl (Dictionary.set new_cookbook key value)
                                                                    else possibleRecipes tl new_cookbook
        in
            possibleRecipes (Dictionary.toList cookbook) Dictionary.empty
        end;*)

    fun possibleRecipes cookbook stock =
        Dictionary.filter (hasEnoughIngredients stock) cookbook;

    fun generateVariants (recipeName, recipeStock) substitutions = 
        let
            fun findEquivalenceClass ingredient [] = [ingredient]
                | findEquivalenceClass ingredient (hd::tl) = if contains hd ingredient (fn (x, y) => ingredientToString x = ingredientToString y) then hd
                                                             else findEquivalenceClass ingredient tl
            fun filterSubstitutions [] acc = acc
                | filterSubstitutions ((ingredient, quantity)::tl) acc = 
                    let
                        fun addQuantityToSubstitutions xs =
                            let
                                fun addQuantityToSubstitutions [] acc = acc
                                    | addQuantityToSubstitutions (hd::tl) acc = addQuantityToSubstitutions tl (acc @ [(hd, quantity)])
                            in
                                addQuantityToSubstitutions xs []
                            end
                        val equivalenceClass = findEquivalenceClass ingredient substitutions
                        val equivalenceClass = addQuantityToSubstitutions equivalenceClass
                    in
                        filterSubstitutions tl (acc @ [equivalenceClass])
                    end
            fun transformToListOfLists xs =
                let 
                    fun transformList [] acc = acc
                        | transformList (hd::tl) acc = transformList tl (acc @ [[hd]])
                    fun transformToListOfLists [] acc = acc
                        | transformToListOfLists (hd::tl) acc = transformToListOfLists tl (acc @ [transformList hd []])
                in
                    transformToListOfLists xs []
                end
            fun generateCombinations x y =
                let 
                    fun generateCombinations [] [] acc = acc
                        | generateCombinations (x::y) [] acc = acc
                        | generateCombinations [] (x::y) acc = acc
                        | generateCombinations (hd1::[]) (hd2::[]) acc = (acc @ [hd1 @ hd2])
                        | generateCombinations (hd1::tl1) (hd2::[]) acc = generateCombinations tl1 y (acc @ [hd1 @ hd2])
                        | generateCombinations (hd1::[]) (hd2::tl2) acc = generateCombinations (hd1::[]) tl2 (acc @ [hd1 @ hd2])
                        | generateCombinations (hd1::tl1) (hd2::tl2) acc = generateCombinations (hd1::tl1) tl2 (acc @ [hd1 @ hd2])
                in
                    generateCombinations x y []
                end
            fun generate [] = []
                | generate (x::[]) = x
                | generate (x::y::[]) = generateCombinations x y
                | generate (x::y::tl) = generate ((generateCombinations x y)::tl)
            fun createRecipeName id = recipeName ^ "#" ^ Int.toString id
            fun makeRecipes [] = [makeIngredient ((createRecipeName 1), makeStock [])]
                | makeRecipes combinations =
                    let 
                        fun makeRecipes [] acc count = acc
                            | makeRecipes (hd::tl) acc count = 
                                let 
                                    val recipeName = makeIngredient (createRecipeName count)
                                    val stock = makeStock hd
                                in
                                    makeRecipes tl (acc @ [makeRecipe (recipeName, stock)]) (count + 1)
                                end
                    in
                        makeRecipes combinations [] 1
                    end

            val substitutions = filterSubstitutions (Dictionary.toList recipeStock) []
            val listOfLists = transformToListOfLists substitutions
            val combinations = generate listOfLists
            val recipes = makeRecipes combinations
        in
            makeCookbook recipes
        end;


    fun cheapestRecipe cookbook pricelist = 
        let
            val prices = Dictionary.map (fn (key, value) => 
                (key, (SOME ((key, value), (priceOfRecipe (key, value) pricelist)) handle NoPriceException => NONE))) cookbook
            val prices = compactMap (Dictionary.values prices)
            val cheapest = min prices (fn ((_, price1), (_, price2)) => price1 < price2)
        in
            case cheapest of
                NONE => NONE
                | SOME (recipe, _) => SOME recipe
        end;
end
