use "solution.sml";
use "unittest.sml";

Control.Print.printLength := 100;
Control.Print.printDepth := 10;
Control.Print.stringDepth := 10000;

val empty = Dictionary.empty;
val a_dict = Dictionary.set 
    (Dictionary.set 
        (Dictionary.set 
            empty "janez" 10) "novak" 20) "tincan" 30;
val no_common_dict = Dictionary.set (Dictionary.set (Dictionary.empty) "josko" 40) "xyz" 50;
val common = Dictionary.set (Dictionary.set (Dictionary.empty) "janez" 40) "xyz" 50;

val salt = Cookbook.makeIngredient "salt"
val sugar = Cookbook.makeIngredient "sugar"
val honey = Cookbook.makeIngredient "honey"
val natreen = Cookbook.makeIngredient "natreen"
val chicken = Cookbook.makeIngredient "chicken"
val pork = Cookbook.makeIngredient "pork"
val beef = Cookbook.makeIngredient "beef"
val veal = Cookbook.makeIngredient "veal"
val salad = Cookbook.makeIngredient "salad"
val egg = Cookbook.makeIngredient "egg"
val redPepper = Cookbook.makeIngredient "red pepper"
val greenPepper = Cookbook.makeIngredient "green pepper"
val milk = Cookbook.makeIngredient "milk"
val flour = Cookbook.makeIngredient "flour"
val pancake = Cookbook.makeIngredient "pancake"
val scrambledEggs = Cookbook.makeIngredient "scrambled eggs"
val padThai = Cookbook.makeIngredient "pad thai"

val stock = Cookbook.makeStock [
    (salt, 3),
    (sugar, 3),
    (chicken, 2),
    (salad, 1),
    (egg, 5),
    (milk, 4),
    (flour, 4)
];

val smallStock = Cookbook.makeStock [
    (salt, 3),
    (egg, 5),
    (milk, 4),
    (flour, 4)
];

val emptyStock = Cookbook.makeStock [];

val pricelist = Cookbook.makePricelist [
    (salt, 0.5),
    (sugar, 0.3),
    (honey, 1.1),
    (natreen, 0.7),
    (chicken, 2.2),
    (pork, 2.8),
    (beef, 2.9),
    (veal, 3.1),
    (salad, 0.8),
    (egg, 0.4),
    (redPepper, 0.5),
    (greenPepper, 0.45),
    (milk, 0.7),
    (flour, 0.9),
    (pancake, 3.5),
    (scrambledEggs, 1.2),
    (padThai, 6.5)
];

val smallPricelist = Cookbook.makePricelist [
    (salt, 0.5),
    (sugar, 0.3),
    (honey, 1.1),
    (natreen, 0.7),
    (chicken, 2.2),
    (pork, 2.8),
    (beef, 2.9),
    (veal, 3.1),
    (salad, 0.8),
    (egg, 0.4),
    (redPepper, 0.5),
    (greenPepper, 0.45),
    (flour, 0.9),
    (pancake, 3.5),
    (scrambledEggs, 1.2),
    (padThai, 6.5)
];

val pricelist2 = Cookbook.makePricelist [
    (veal, 55.0)
];

val emptyRecipe = Cookbook.makeRecipe (pancake, Cookbook.makeStock []);

val pancakeRecipe = Cookbook.makeRecipe (pancake, Cookbook.makeStock [
    (salt, 1),
    (sugar, 1),
    (egg, 2),
    (milk, 1)
]);

val hugePancakeRecipe = Cookbook.makeRecipe (pancake, Cookbook.makeStock [
    (salt, 10),
    (sugar, 1),
    (egg, 2),
    (milk, 1)
]);

val hugePancakeRecipe2 = Cookbook.makeRecipe (pancake, Cookbook.makeStock [
    (salt, 1),
    (sugar, 1),
    (egg, 2),
    (milk, 10)
]);

val padThaiRecipe = Cookbook.makeRecipe (padThai, Cookbook.makeStock [
    (salt, 1),
    (chicken, 1),
    (sugar, 1),
    (redPepper, 1),
    (flour, 1)
]);

val scrambledEggsRecipe = Cookbook.makeRecipe (scrambledEggs, Cookbook.makeStock [
    (egg, 1)
]);

val beefRecipe = Cookbook.makeRecipe (beef, Cookbook.makeStock [
    (beef, 2)
]);

val cookbook = Cookbook.makeCookbook [
    pancakeRecipe,
    scrambledEggsRecipe,
    padThaiRecipe
];

val cookbook2 = Cookbook.makeCookbook [
    hugePancakeRecipe
];

val emptyCookbook = Cookbook.makeCookbook [];

val smallPricelist = Cookbook.makePricelist [
    (salt, 0.5),
    (sugar, 0.3),
    (honey, 1.1),
    (natreen, 0.7)
];

(************* Utils tests *************)

fun int_sort (xs: int list) = sort xs Int.<;
fun str_sort (xs: string list) = sort xs String.<;

val _ = (
    test ("sort", [
        assert_eq (int_sort [], []),
        assert_eq (int_sort [2, 3, 1, 5, 0], [0, 1, 2, 3, 5]),
        assert_eq (str_sort ["b", "ab", "bca", "e", "aab", "cce"], 
                            ["aab", "ab", "b", "bca", "cce", "e"])
    ]);

    test ("min", [
        assert_eq (min [] (fn _ => true), NONE),
        assert_eq (min [1] Int.<, SOME 1),
        assert_eq (min [1, 2, 3] Int.<, SOME 1),
        assert_eq (min [(1, "a"), (2, "b"), (3, "c")] (fn ((x, _), (y, _)) => x < y), SOME (1, "a"))
    ]);

    test ("contains", [
        assert_false (contains [] 0 (fn _ => true)),
        assert (contains [1] 1 (fn (x, y) => x = y)),
        assert (contains [1, 2, 3] 1 (fn (x, y) => x = y)),
        assert (contains [1, 2, 3] 3 (fn (x, y) => x = y))
    ]);

    test ("compactMap", [
        assert_eq (compactMap [], []),
        assert_eq (compactMap [SOME 1, SOME 2], [1, 2]),
        assert_eq (compactMap [SOME 1], [1]),
        assert_eq (compactMap [SOME 1, NONE, SOME 2, NONE, NONE, SOME 3], [1, 2, 3])
    ])
);

(************* Dictionary tests *************)

val _ = (
    test ("size", [
        assert_eq (Dictionary.size Dictionary.empty, 0),
        assert_eq (Dictionary.size a_dict, 3)
    ]);

    test ("isEmpty", [
        assert (Dictionary.isEmpty empty),
        assert_false (Dictionary.isEmpty a_dict)
    ]); 

    test ("get", [
        assert_eq (Dictionary.get a_dict "janez", SOME 10),
        assert_eq (Dictionary.get a_dict "novak", SOME 20),
        assert_eq (Dictionary.get a_dict "not present", NONE),
        assert_eq (Dictionary.get empty "x", NONE)
    ]);

    test ("set (should_override)", [
       assert_eq (Dictionary.size (Dictionary.set a_dict "janez" 15), 3),
       assert_eq (Dictionary.get (Dictionary.set a_dict "janez" 15) "janez", SOME 15)
    ]);

    test ("get_or_default", [
        assert_eq (Dictionary.getOrDefault a_dict "janez" 999, 10),
        assert_eq (Dictionary.getOrDefault a_dict "novak" 999, 20),
        assert_eq (Dictionary.getOrDefault a_dict "not present" 999, 999),
        assert_eq (Dictionary.getOrDefault empty "x" 999, 999)
    ]);

    test ("remove", [
        assert_eq (Dictionary.size common, 2),
        assert_eq (Dictionary.size (Dictionary.remove common "janez"), 1),
        assert_eq (Dictionary.size (Dictionary.remove empty "janez"), 0)
    ]);

    test ("exists", [
        assert (Dictionary.exists a_dict "janez"),
        assert (Dictionary.exists a_dict "novak"),
        assert_false (Dictionary.exists a_dict "x"),
        assert_false (Dictionary.exists a_dict "y")
    ]);

    test ("keys", [
        assert_eq (Dictionary.keys a_dict, ["janez", "novak", "tincan"]),
        assert_eq (Dictionary.keys empty, [])
    ]);

    test ("values", [
        assert_eq (Dictionary.values a_dict, [10, 20, 30]),
        assert_eq (Dictionary.values empty, [])
    ]);

    test ("to_list", [
        assert_eq (Dictionary.toList a_dict, [("janez", 10), 
                                              ("novak", 20), 
                                              ("tincan", 30)]),
        assert_eq (Dictionary.toList empty, [])
    ]);

    test ("from_list", [
        assert_eq (Dictionary.toList (Dictionary.fromList (Dictionary.toList a_dict)), 
            [("janez", 10), 
             ("novak", 20), 
             ("tincan", 30)]),
        assert (Dictionary.isEmpty (Dictionary.fromList []))
    ]);

    test ("merge", [
        assert_eq (Dictionary.toList (Dictionary.merge a_dict empty), [("janez", 10), 
                                                                       ("novak", 20), 
                                                                       ("tincan", 30)]),
        assert_eq (Dictionary.toList (Dictionary.merge a_dict no_common_dict), [("janez", 10), 
                                                                                ("novak", 20), 
                                                                                ("tincan", 30),
                                                                                ("josko", 40),
                                                                                ("xyz", 50)]),
        assert_eq (Dictionary.toList (Dictionary.merge a_dict common), [("janez", 40), 
                                                                        ("novak", 20), 
                                                                        ("tincan", 30),
                                                                        ("xyz", 50)])
    ]);

    test ("filter", [
        assert_eq (Dictionary.toList (Dictionary.filter (fn (key, value) => value <= 20) a_dict), [("janez", 10), 
                                                                                                   ("novak", 20)])
    ]);

    test ("map", [
        assert_eq (Dictionary.toList (Dictionary.map (fn (key, value) => (key, value + 50)) a_dict), [("janez", 60), 
                                                                                                      ("novak", 70), 
                                                                                                      ("tincan", 80)])
    ])
);

(************* Cookbook tests *************)

val _ = (
    test ("stockToString", [
        assert_eq (Cookbook.stockToString stock, 
            "chicken: 2\negg: 5\nflour: 4\nmilk: 4\nsalad: 1\nsalt: 3\nsugar: 3")
    ]);

    test ("pricelistToString", [
        assert_eq (Cookbook.pricelistToString smallPricelist, 
            "honey: 1.1\nnatreen: 0.7\nsalt: 0.5\nsugar: 0.3")
    ]);

    test ("recipeToString", [
        assert_eq (Cookbook.recipeToString pancakeRecipe, 
            "=== pancake ===\negg: 2\nmilk: 1\nsalt: 1\nsugar: 1"),
        assert_eq (Cookbook.recipeToString emptyRecipe, 
            "=== pancake ===\n")
    ]);

    test ("cookbookToString", [
        assert_eq (Cookbook.cookbookToString cookbook, 
            "=== pad thai ===\nchicken: 1\nflour: 1\nred pepper: 1\nsalt: 1\nsugar: 1\n\n=== pancake ===\negg: 2\nmilk: 1\nsalt: 1\nsugar: 1\n\n=== scrambled eggs ===\negg: 1"),
        assert_eq (Cookbook.cookbookToString emptyCookbook, "")
    ]);

    test ("hasEnoughIngredients", [
        assert (Cookbook.hasEnoughIngredients stock pancakeRecipe),
        assert (Cookbook.hasEnoughIngredients stock scrambledEggsRecipe),
        assert_false (Cookbook.hasEnoughIngredients stock padThaiRecipe),
        assert_false (Cookbook.hasEnoughIngredients stock hugePancakeRecipe),
        assert_false (Cookbook.hasEnoughIngredients stock hugePancakeRecipe2),
        assert_false (Cookbook.hasEnoughIngredients stock beefRecipe)
    ]);

    test ("cook", [
        assert_eq (Cookbook.stockToString (Cookbook.cook padThaiRecipe stock), 
            "chicken: 1\negg: 5\nflour: 3\nmilk: 4\npad thai: 1\nsalad: 1\nsalt: 2\nsugar: 2"),
        assert_eq (Cookbook.stockToString (Cookbook.cook scrambledEggsRecipe stock), 
            "chicken: 2\negg: 4\nflour: 4\nmilk: 4\nsalad: 1\nsalt: 3\nscrambled eggs: 1\nsugar: 3"),
        assert_eq (Cookbook.stockToString 
            (Cookbook.cook scrambledEggsRecipe (Cookbook.cook scrambledEggsRecipe stock)), 
            "chicken: 2\negg: 3\nflour: 4\nmilk: 4\nsalad: 1\nsalt: 3\nscrambled eggs: 1\nsugar: 3"),
        assert_eq (Cookbook.stockToString (Cookbook.cook pancakeRecipe stock),
            "chicken: 2\negg: 3\nflour: 4\nmilk: 3\npancake: 1\nsalad: 1\nsalt: 2\nsugar: 2"),
        assert_eq (Cookbook.stockToString (Cookbook.cook hugePancakeRecipe stock),
            "chicken: 2\negg: 3\nflour: 4\nmilk: 3\npancake: 1\nsalad: 1\nsugar: 2")
    ]);

    test ("priceOfStock", [
        assert_eq_real (Cookbook.priceOfStock stock pricelist, 16.0),
        assert_raises ((fn _ => Cookbook.priceOfStock stock pricelist2), Cookbook.NoPriceException)
    ]);

    test ("priceOfRecipe", [
        assert_eq_real (Cookbook.priceOfRecipe beefRecipe pricelist, 5.8),
        assert_eq_real (Cookbook.priceOfRecipe padThaiRecipe pricelist, 4.4),
        assert_eq_real (Cookbook.priceOfRecipe scrambledEggsRecipe pricelist, 0.4)
    ]);

    test ("missingIngredients", [
        assert_eq (Cookbook.stockToString (Cookbook.missingIngredients padThaiRecipe stock), 
            "red pepper: 1"),
        assert_eq (Cookbook.stockToString (Cookbook.missingIngredients padThaiRecipe smallStock),
            "chicken: 1\nred pepper: 1\nsugar: 1"),
        assert_eq (Cookbook.stockToString (Cookbook.missingIngredients pancakeRecipe stock), "")
    ]);

    test ("possibleRecipes", [
        assert_eq (Cookbook.cookbookToString (Cookbook.possibleRecipes cookbook stock), 
            "=== pancake ===\negg: 2\nmilk: 1\nsalt: 1\nsugar: 1\n\n=== scrambled eggs ===\negg: 1"),
        assert_eq (Cookbook.cookbookToString (Cookbook.possibleRecipes emptyCookbook stock), "")
    ]);

    test ("generateVariants", [
        assert_eq (Cookbook.cookbookToString (Cookbook.generateVariants (Cookbook.makeRecipe (salt, Cookbook.makeStock [])) [[salt, honey], [chicken]]), 
            "=== salt#1 ===\n"),
        assert_eq (Cookbook.cookbookToString (Cookbook.generateVariants padThaiRecipe []), 
            "=== pad thai#1 ===\nchicken: 1\nflour: 1\nred pepper: 1\nsalt: 1\nsugar: 1"),
        assert_eq (Cookbook.cookbookToString (Cookbook.generateVariants padThaiRecipe [[salt, honey], [chicken]]), 
            "=== pad thai#1 ===\nchicken: 1\nflour: 1\nred pepper: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#2 ===\nchicken: 1\nflour: 1\nhoney: 1\nred pepper: 1\nsugar: 1"),
        assert_eq (Cookbook.cookbookToString (Cookbook.generateVariants padThaiRecipe [[salt, honey, natreen], [pork, beef, chicken], [redPepper, greenPepper], [veal, egg]]), 
            "=== pad thai#1 ===\nflour: 1\npork: 1\nred pepper: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#10 ===\nbeef: 1\nflour: 1\ngreen pepper: 1\nhoney: 1\nsugar: 1\n\n=== pad thai#11 ===\nchicken: 1\nflour: 1\nhoney: 1\nred pepper: 1\nsugar: 1\n\n=== pad thai#12 ===\nchicken: 1\nflour: 1\ngreen pepper: 1\nhoney: 1\nsugar: 1\n\n=== pad thai#13 ===\nflour: 1\nnatreen: 1\npork: 1\nred pepper: 1\nsugar: 1\n\n=== pad thai#14 ===\nflour: 1\ngreen pepper: 1\nnatreen: 1\npork: 1\nsugar: 1\n\n=== pad thai#15 ===\nbeef: 1\nflour: 1\nnatreen: 1\nred pepper: 1\nsugar: 1\n\n=== pad thai#16 ===\nbeef: 1\nflour: 1\ngreen pepper: 1\nnatreen: 1\nsugar: 1\n\n=== pad thai#17 ===\nchicken: 1\nflour: 1\nnatreen: 1\nred pepper: 1\nsugar: 1\n\n=== pad thai#18 ===\nchicken: 1\nflour: 1\ngreen pepper: 1\nnatreen: 1\nsugar: 1\n\n=== pad thai#2 ===\nflour: 1\ngreen pepper: 1\npork: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#3 ===\nbeef: 1\nflour: 1\nred pepper: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#4 ===\nbeef: 1\nflour: 1\ngreen pepper: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#5 ===\nchicken: 1\nflour: 1\nred pepper: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#6 ===\nchicken: 1\nflour: 1\ngreen pepper: 1\nsalt: 1\nsugar: 1\n\n=== pad thai#7 ===\nflour: 1\nhoney: 1\npork: 1\nred pepper: 1\nsugar: 1\n\n=== pad thai#8 ===\nflour: 1\ngreen pepper: 1\nhoney: 1\npork: 1\nsugar: 1\n\n=== pad thai#9 ===\nbeef: 1\nflour: 1\nhoney: 1\nred pepper: 1\nsugar: 1")
    ]);

    test ("cheapestRecipe", [
        assert_eq (Cookbook.recipeToString (valOf (Cookbook.cheapestRecipe cookbook pricelist)), 
            Cookbook.recipeToString scrambledEggsRecipe),
        assert_false (isSome (Cookbook.cheapestRecipe cookbook2 smallPricelist)),
        assert_false (isSome (Cookbook.cheapestRecipe emptyCookbook pricelist))
    ])
);

OS.Process.exit(OS.Process.success);
