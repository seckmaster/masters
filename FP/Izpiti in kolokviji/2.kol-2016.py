def decor(f):
  def wrapper(*args):
    if cache:
      yield cache
    else:
      c = f(*args)
      cache.add(c)
      yield cache
  cache = set()
  return wrapper

@decor
def kvadrat(x):
  return x * x

a = kvadrat(10)
print(a)

# def preveri(f):
#   def wrapper(*args, **kwargs):
#     if len(args) == 3 and "n" in kwargs:
#       f(*args, **kwargs)
#       print("Funkcija izvedena")
#     else:
#       print("Argumenti niso ustrezni")
#   return wrapper
#
# @preveri
# def f(a, b, c, **kwargs):
#   pass
#
# f()
# f(1, "eevee", 3)
# f(1, "eevee", n=5)
# f(1, "eevee", 3, n=5)
#
# from itertools import permutations
#
def enkrat(f):
  def wrapper(*args, **kwargs):
    if args in arguments:
      print("Napaka")
    else:
      print("Izvedena")
      f(*args, **kwargs)
  arguments = set()
  return wrapper
#
# @enkrat
# def f(*args):
#   pass
#
# f(1, 2, 3)
# f(3, 2, 1)
# f(1, 3, 2)
# f(3, 3, 2)
#
# def curry(f):
#   def wrapper(*args, **kwargs):
#     if len(args) == 3:
#       print("Curryfied parametrov: 1")
#       return f(*args)
#     if len(args) == 2:
#       print("Curryfied parametrov: 2")
#       def curry(a):
#         return f(*args, a)
#       return curry
#     if len(args) == 1:
#       def curry(*args2):
#         if len(args2) == 2:
#           print("Curryfied parametrov: 2")
#           return f(*args, *args2)
#         elif len(args2) == 1:
#           print("Curryfied parametrov: 3")
#           def inner_curry(a):
#             return f(*args, *args2, a)
#           return inner_curry
#       return curry
#   return wrapper
#
# @curry
# def f(a, b, c):
#   return a + b + c
#
# print(f(1, 2, 3))
# print(f(1)(2, 3))
# print(f(1, 2)(3))
# print(f(1)(2)(3))
#
#
# def decor(*args):
#   def out_wrapp(fun):
#     def wrapp(*args2):
#       print("kle", args2)
#     print("kle2", fun)
#     return wrapp
#   print("kle3", args)
#   return out_wrapp
#
# print()
# @decor(10)
# def f():
#   pass
#
# f(10)
#
# def genstevec(max):
#   i = 0
#   while i < max:
#     vnos = yield i
#     if vnos is None:
#       i += 1
#     elif vnos >= max:
#       raise StopIteration
#     else:
#       i = vnos
#
# print("....")
# x = genstevec(10)
# print(next(x))
# print(next(x))
# print(next(x))
# x.send(8)
# print(next(x))