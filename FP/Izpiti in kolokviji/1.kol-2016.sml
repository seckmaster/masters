fun f (a, b) (c::d) e =
	if (!b) = (c mod 2 = 0)
	then fn x => e
	else fn (y, z) => valOf (z) andalso a mod 2 = 1;


fun stranice a b fa fb fs ffin =
	ffin (Real.fromInt(fs (fa a) (fb b)));

fun obseg a b = stranice a b (fn x => 2 * x) (fn x => 2 * x) (fn x => fn y => x + y) (fn x => x);
fun ploscina a b = stranice a b (fn x => x) (fn x => x) (fn x => fn y => x + y) (fn x => x);
fun diagonala a b = stranice a b (fn x => x * x) (fn x => x * x) (fn x => fn y => x + y) (fn x => Math.sqrt x);

diagonala 10 20;

fun map f a =
	List.foldl (fn (x, acc) => acc @ [f x]) [] a;

fun map f sez = 
    let 
        val fx = fn (el, acc) => acc @ [f el] 
    in 
        List.foldl fx [] sez
    end;

map (fn x => x * x) [1, 2, 3, 4];

fun map f a =
	List.foldr (fn (x, acc) => f x::acc) [] a;

map (fn x => x * x) [1, 2, 3, 4];

datatype 'a zaporedje = Predmet of (int * 'a) * ('a zaporedje) | Konec;

fun prestej ocene =
	let 
		fun prestej (ocene, acc) =
			case ocene of 
				Konec => acc
				| Predmet (_,preostale) => prestej (preostale, acc + 1)
	in
		prestej (ocene, 0)
	end;

val ocene = Predmet ((10, "FP"), Predmet ((8, "MAT"), Konec));
prestej ocene;

fun prestej zap =
	let 
		fun prestej (Predmet (_,z), acc) = prestej (z, acc + 1)
			| prestej (Konec, acc) = acc
	in 
		prestej (zap, 0)
	end;
prestej ocene;

OS.Process.exit(OS.process.success);
