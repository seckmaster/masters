fun prva (a, b) c =
	a::c;

fun druga (a, b) c =
	{a=b andalso c, b=c, c=a};

fun tretja (a, b, c) =
	([c a, c b], c);

fun cetrta [a, b, c] =
	let val a = 3 val b = 4
	in c a = c b end;

fun fun1 sez: ('a * 'a) list =
	case sez of
    	g1::g2::r => g2::g1::(fun1 r)
    	| _ => [];

fun fun2 sez = 
	case sez of
    	(g1,g2)::r => (g2,g1)::(fun2 r)
    	| _ => [];

fun splosna xs f = 
	let 
		fun splosna (g1::g2::t, acc) = 
			let 
				val (a, b) = f (g1, g2)
			in 
				splosna (t, acc @ [a, b])
			end
			| splosna ([], acc) = acc
	in 
		splosna (xs, [])
	end;

fun fun1nova sez: ('a * 'a) list = splosna sez (fn (a, b) => (b, a));
fun fun2nova sez = splosna sez (fn ((a1, b1), (a2, b2)) => ((b1, a1), (b2, a2)));

fun1 [(1,10),(2,11),(3,12),(4,13)];
fun1nova [(1,10),(2,11),(3,12),(4,13)];
fun2 [(1,10),(2,11),(3,12),(4,13)];
fun2nova [(1,10),(2,11),(3,12),(4,13)];

fun f xs =
	case xs of 
		[] => []
		| g::r => case g of 
			NONE => f r
			| SOME (a, b) => (if a > b then SOME (a + b) else NONE)::f r;

 f [SOME (2, 1), NONE, SOME (3,3)];

 fun f xs =
	case xs of 
		[] => []
		| SOME (a, b)::r => (if a > b then SOME (a + b) else NONE)::f r
		| NONE::r => f r;
f [SOME (2, 1), NONE, SOME (3,3)];

(* 4 NALOGA *)

(* to ni narjeno z repno rekurzijo *)
fun vsakdrug xs =
	case xs of 
		[] => []
		| g1::g2::r => g1 + 10::g2::vsakdrug r
		| g1::[] => g1 + 10::[];

(* z repno rekurzijo. Vrjamm da se da lepse, sam fuck it *)
fun vsakdrug sez =
    let
      fun pomozna(sez, acc, add) =
        case sez of
           nil => acc
         | glava::rep => if add = 1 then pomozna(rep, acc@[glava + 10], add -1)
                         else pomozna(rep, acc@[glava ], add + 1)
    in
      pomozna(sez, [], 1)
    end;

vsakdrug [4,5,6,7,8,9,8,7,6];

fun vsakdrug xs =
	let 
		fun vsakdrug (g1::g2::t, acc) = vsakdrug (t, acc @ [g1 + 10, g2])
			| vsakdrug (g1::[], acc) = acc @ [g1 + 10]
			| vsakdrug ([], acc) = acc
	in 
		vsakdrug (xs, [])
	end;

vsakdrug [4,5,6,7,8,9,8,7,6];

datatype vrtnarstvo =
	Obrezi of {ime: string, cas: int}
	| Zalij of {ime: string, kolicina: int}
	| Obcuduj of {ime: string, cas: int}
	| Meditiraj of {cas: int}
	| Vonjaj of {ime: string};

val dejavnosti = [Obrezi {ime="vrtnica", cas=5}, 
                  Zalij {ime="vrtnica", kolicina=1}, 
                  Zalij {ime="drevo", kolicina=5}, 
                  Vonjaj {ime="vrtnica"}, 
                  Meditiraj{cas=10}];

fun evalvirajVrt xs =
	let fun eval (xs, c, k) =
		case xs of 
			[] => {cas=c, kolicina=k}
			| Obrezi (dejavnost)::r => eval (r, c + #cas dejavnost, k)
			| Zalij dejavnost::r => eval (r, c, k + #kolicina dejavnost)
			| Obcuduj dejavnost::r => eval (r, c + #cas dejavnost, k)
			| Meditiraj dejavnost::r => eval (r, c + #cas dejavnost, k)
			| Vonjaj _::r => eval (r, c, k)
	in
		eval (xs, 0, 0)
	end;

evalvirajVrt(dejavnosti);

fun evalvirajVrt xs =
	let fun eval (xs, c, k) =
		case xs of 
			[] => {cas=c, kolicina=k}
			| Obrezi {ime, cas}::r => eval (r, c + cas, k)
			| Zalij {ime, kolicina}::r => eval (r, c, k + kolicina)
			| Obcuduj {ime, cas}::r => eval (r, c + cas, k)
			| Meditiraj {cas}::r => eval (r, c + cas, k)
			| Vonjaj _::r => eval (r, c, k)
	in
		eval (xs, 0, 0)
	end;

evalvirajVrt(dejavnosti);

OS.Process.exit(OS.process.success);
