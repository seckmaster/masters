signature LogSig1 = 
sig
	type vrednost
	val default : vrednost
	val izdelaj : vrednost -> vrednost
	val obdelaj : vrednost -> vrednost
end

structure Logika :> LogSig1 =
struct
	type vrednost = bool * int
	exception Napaka
	val default = (false, 0);
	fun izdelaj (x, y) = if y < 10 then (x, y) else raise Napaka
	fun obdelaj (x, y) = (not x, if y mod 2 = 0 then y - 1 else y + 1)
end;

Logika.izdelaj Logika.default;