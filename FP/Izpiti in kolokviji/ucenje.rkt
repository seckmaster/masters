#lang racket

(define (potence_tok [start 2])
  (define (promise el)
    (cons (lambda () (promise (/ el 2))) 
          (cons el  (lambda () (promise (* el 2))))))
  (cons (lambda () (promise (/ start 2)))
        (cons start 
              (lambda () (promise (* start 2))))))

(define potence (potence_tok))

(define-syntax trenutni
  (syntax-rules ()
    [(trenutni tok) (car (cdr tok))]))

(define-syntax prejsnji
  (syntax-rules ()
    [(prejsnji tok) ((car tok))]))

(define-syntax naslednji
  (syntax-rules ()
    [(prejsnji tok) ((cdr (cdr tok)))]))

(trenutni potence)
(prejsnji potence)
(naslednji potence)
(trenutni (prejsnji potence))
(naslednji (naslednji (naslednji (naslednji potence))))
(trenutni (naslednji (naslednji (naslednji (naslednji potence)))))

(define (koncni_tok xs)
  (define (consume_row row col xss)
    (cons (if (null? xss)
              null
              (list row col (car xss)))
          (lambda () (consume_row row
			                      (+ col 1) 
			                      (cdr xss)))))
  (define (promise xs row col)
    (if (null? xs)
        (cons null (lambda () (promise xs row col)))
        (let ([x (consume_row row col (car xs))])
          (if (null? (car x))
              (promise (cdr xs) (+ 1 row) 1)
              (cons (car x)
                    (lambda () (begin
                                 (define y ((cdr x)))
                                	(if (null? (car y))
                                    	(promise (cdr xs) (+ 1 row) 1)
                                    	y))))))))
  (promise xs 1 1))

(define tok (koncni_tok (list (list 1 2) (list 3 4) (list 5 6))))
(car tok)
(car ((cdr tok)))
(car ((cdr ((cdr tok)))))
(car ((cdr ((cdr ((cdr tok)))))))

(define (consume stream)
  (cond [(not (null? (car stream))) (begin (displayln (car stream))
                                           (consume ((cdr stream))))]))
; (consume tok)