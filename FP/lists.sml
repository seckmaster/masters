fun len(list): int =
  if null list
  then 0
  else 1 + len(tl list);

len([1, 2, 3]);
len([]);

fun sum(lst: int list): int =
  if null lst
  then 0
  else hd lst + sum(tl lst);

sum([]);
sum(1::2::3::[]);
sum(1::[2, 3]);
sum([1, 2, 3]);

fun nth_element(lst, n: int): int option =
  if null lst
  then NONE
  else if n = 0
  then SOME (hd lst)
  else nth_element(tl lst, n - 1);

isSome (nth_element([], 0));
nth_element([], 2);
nth_element([1, 2, 3], 0);
nth_element([1, 2, 3], 1);
nth_element([1, 2, 3], 2);
nth_element([1, 2, 3], 3);
nth_element([1, 2, 3, 4, 5, 6], 5);

fun append(lst_a, lst_b) =
  if null lst_a
  then lst_b
  else append(tl lst_a, hd lst_a::lst_b); (* `lst_a` get's revered :( *)

append([], []); (* Warning: type cannot be evaluated *)
append([], [1, 2]);
append([1, 2], []);
append(["a", "b", "c"], ["e", "f", "g"]);

fun zip(lst_a, lst_b) =
  if null lst_b orelse null lst_a
  then []
  else (hd lst_a, hd lst_b)::zip(tl lst_a, tl lst_b);

zip([1, 2], [3, 4, 5]);
zip([1, 2, 3], [5, 6]);
zip([1, 2, 3], [4, 5, 6]);

fun sum_pair(lst_a: int list, lst_b: int list): int list =
  if null lst_b orelse null lst_b
  then []
  else (hd lst_a + hd lst_b)::sum_pair(tl lst_a, tl lst_b);

sum_pair([], []);
sum_pair([6, 7, 8], [4, 3, 2]);
sum_pair([4, 5, 6], [6, 5]);

fun sum_pair_tuples(lst: (int * int) list): int list =
  if null lst
  then []
  else (#1 (hd lst) + #2 (hd lst))::sum_pair_tuples(tl lst);

sum_pair_tuples([]);
sum_pair_tuples([(1, 9), (2, 8), (3, 7)]);

fun filter_grades(grades: (string * int) list): string list =
  if null grades then []
  else if #2 (hd grades) > 6
  then #1 (hd grades)::filter_grades(tl grades)	
  else filter_grades(tl grades);

filter_grades([]);
filter_grades([("Mat", 6), ("Fiz", 8), ("Slo", 5), ("FP", 5), ("APS 2", 10)]);

fun avg(lst) =
let 
  fun len_and_sum(lst) = (* Returns sum of all elements and their count *)
    if null lst then (0, 1)
	else 
    let
      val res = len_and_sum(tl lst) 
	in
	  (hd lst + #1 res, #2 res + 1)
    end
  val result = len_and_sum lst
in
  Real.fromInt(#1 result) / Real.fromInt(#2 result)
end;

avg [];
avg [10, 20, 30];


fun append(lst, a) = 
  let
    fun insert(lst, x, n) =
      if null lst then x::[]
      else if n = 0 then x::lst
      else (hd lst)::insert(tl lst, x, n - 1)
    fun len lst =
      if null lst then 0
      else 1 + len(tl lst)
  in
    insert (lst, a, len lst)
  end;

append ([], 10);
append ([1, 2, 3], 10);
append ([1], 10);













