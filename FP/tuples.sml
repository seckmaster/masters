fun sestej(a: int * int): int =
  #1 a + #2 a;

sestej(10, 20);

fun obrni(a: int * int): int * int =
  (#2 a, #1 a);

obrni(20, 30);

fun prepleti(a: int * int * int, b: int * int * int): int * int * int * int * int * int =
  (#1 a, #1 b, #2 a, #2 b, #3 a, #3 b);

prepleti((1, 2, 3), (4, 5, 6));

fun sortiraj(a: int* int): int * int =
  if #1 a < #2 a
  then a
  else obrni(a);

sortiraj(10, 20);
sortiraj(20, 10);





