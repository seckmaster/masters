def emso_verify(emso):
  """
  Accepts an iterable of at least 12 digits and returns the number
  as a 13 digit string with a valid 13th control digit.
  Details about computation in
  http://www.uradni-list.si/1/objava.jsp?urlid=19998&stevilka=345
  """
  emso_factor_map = [7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2]
  emso_digit_list = [int(x) for x in emso]
  emso_sum = sum([emso_digit_list[i] * emso_factor_map[i] for i in range(12)])
  control_digit = 0 if emso_sum % 11 == 0 else 11 - (emso_sum % 11)
  return str(emso)[:12] + str(control_digit)

emso_verify("2711000500227")