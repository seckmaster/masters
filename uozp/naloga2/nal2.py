import numpy as np
from unidecode import unidecode
from collections import Counter
from matplotlib import pyplot

class Matrix:
  def __init__(self, k, countries, preproccess_enabled=False):
    self.k = k
    self.countries = countries
    self.preproccess_enabled = preproccess_enabled

  def generate_distance_matrix(self):
    def distance_matrix(matrix):
      distance_matrix = np.zeros(shape=(len(matrix), len(matrix)))
      for i, vec_a in enumerate(matrix):
        for j, vec_b in enumerate(matrix):
          distance = 0 if i == j else calculate_cosine_distance(i, j, matrix)
          distance_matrix[i, j] = distance
          distance_matrix[j, i] = distance
      return distance_matrix

    def calculate_cosine_distance(a, b, matrix):
      def generate_pairs(a, b):
        intersect = set(a.keys()) & set(b.keys())
        a = np.fromiter((a[key] for key in intersect), float)
        b = np.fromiter((b[key] for key in intersect), float)
        return a, b

      def calculate_distance(x, y):
        distance = np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))
        return 1 - distance

      x, y = list(generate_pairs(matrix[a], matrix[b]))
      return calculate_distance(x, y)

    matrix = []
    for i, file in enumerate(self.countries):
      count = Counter(
        self.__generate_k_terms(
          self.__preprocess(
            self.__open_file(file))))
      matrix.append(dict(count))
    return distance_matrix(matrix)

  def __open_file(self, file):
    with open(file) as handle:
      return handle.read()

  def __preprocess(self, string):
    in_progress = unidecode(string).lower()
    in_progress = in_progress.replace("\n", " ")

    if not self.preproccess_enabled: return in_progress
    to_remove = [",", "'", "\"", "(", ")", ".", "<", ">",
                 ";", ":", "-", "!", "*", "#", "[", "]",
                 "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    for i in to_remove:
      in_progress = in_progress.replace(i, "")
    return in_progress

  def __generate_k_terms(self, string):
    for i in range(0, int(len(string) - (self.k - 1))):
      yield string[i: i + self.k]


class KMemoidsClustering:
  def __init__(self, distance_matrix, debug_print=False):
    self.distance_matrix = distance_matrix
    self.debug_print = debug_print

  def clustering(self, n_memoids):
    memoids, clusters, cost = self._calculate_memoids(n_memoids)
    return memoids, clusters, cost

  def _calculate_memoids(self, n_memoids):
    def choose_random_memoids(number_of_memoids):
      return np.random.choice(np.fromiter(range(len(self.distance_matrix)), int),
                              number_of_memoids,
                              replace=False)

    def associate_vectors_with_memoids(memoids):
      configuration = [[] for _ in range(len(memoids))]
      for vector_idx, _ in enumerate(self.distance_matrix):
        closest_memoid_idx = closest_memoid(vector_idx, memoids)[1]
        configuration[closest_memoid_idx].append(vector_idx)
      return configuration

    def closest_memoid(vector_idx, memoids):
      distances = np.fromiter((self._distance(vector_idx,
                                              memoid_idx)
                               for memoid_idx in memoids),
                              float)
      return (np.min(distances, axis=0),
              np.argmin(distances, axis=0))

    def configuration_cost(configuration, memoids):
      return sum([sum(self._distance(vector, memoids[memoid_idx]) / (1 if len(vectors) == 1 else len(vectors) - 1)
                      for vector in vectors)
                  for memoid_idx, vectors in enumerate(configuration)])

    memoids = choose_random_memoids(n_memoids)
    self._debug_print("Memoids:", memoids)

    should_continue = True
    i = 0

    while should_continue:
      should_continue = False
      i += 1

      configuration = associate_vectors_with_memoids(memoids)
      current_distance_cost = configuration_cost(configuration, memoids)
      minimal_distance_cost = current_distance_cost

      self._debug_print("Current cost:", current_distance_cost)

      for memoid_idx, associated_vectors in enumerate(configuration):
        current_memoid = memoids[memoid_idx]

        for idx, vector_idx in enumerate(associated_vectors):
          memoids[memoid_idx] = vector_idx

          new_configuration = associate_vectors_with_memoids(memoids)
          new_distances_cost = configuration_cost(new_configuration, memoids)

          self._debug_print(memoid_idx, vector_idx,
                            current_distance_cost,
                            new_distances_cost,
                            memoids)
          if new_distances_cost >= current_distance_cost:
            memoids[memoid_idx] = current_memoid
          else:
            # improvement
            current_distance_cost = new_distances_cost
            current_memoid = vector_idx
            self._debug_print("Improvement found:", memoids, current_distance_cost)

      if minimal_distance_cost > current_distance_cost:
        self._debug_print("New best:", memoids, current_distance_cost,
                          "Improvement:", minimal_distance_cost - current_distance_cost)
        minimal_distance_cost = current_distance_cost
        should_continue = True

    self._debug_print("Finished in", i, "iterations. Cost:",
                      minimal_distance_cost, memoids)
    return (memoids,
            configuration,
            minimal_distance_cost)

  def silhouette_cost(self, clusters):
    def average_cost(vector, cluster):
      return sum(self._distance(vector, other_vector)
                 for other_vector in cluster) / len(cluster)

    def closest_cluster(vector, clusters):
      return min(average_cost(vector, cluster)
                 for cluster in clusters)

    def calculate_cost(a, b):
      return (b - a) / max(a, b)

    costs = []
    for cluster in clusters:
      for vector in cluster:
        other_clusters = [c for c in clusters if vector not in c]
        cost = calculate_cost(average_cost(vector, cluster),
                              closest_cluster(vector, other_clusters))
        costs.append(cost)
    return sum(costs) / len(costs)

  def _distance(self, a, b):
    return self.distance_matrix[a][b]

  def _debug_print(self, *args):
    if not self.debug_print: return
    print(args)


LANGUAGES = {
  # SLAVIC
  "slv": "slovenščina",
  "src1": "hvraščina",
  "mkj": "makedonščina",
  "pql": "poljščina",
  "rus": "ruščina",
  "ruw": "beloruščina",
  "slo": "slovaščina",
  "src4": "bosanščina",
  "src5": "srbščina",
  "ukr": "ukrajinščina",
  "czc": "češčina",
  # GERMANIC
  "ger": "nemščina",
  "dut": "nizozemščina",
  "swd": "švedščina",
  "dns": "danščina",
  "nrn": "norveščina",
  "ice": "islandščina",
  # ROMANIC
  "rum": "romunščina",
  "itn": "italijanščina",
  "por": "portugalščina",
  "spn": "španščina",
  "frn": "francoščina",
  "cln": "katalonščina",
  # BALTIC
  "lit": "litovščina",
  "lat": "latvijščina",
  # GREEK
  "grk": "grščina",
}

all_languages = list(LANGUAGES.keys())
file_names = ["ready/" + language + ".txt" for language in all_languages]

print("All languages:", len(all_languages))
for i, j in enumerate(all_languages):
  print(LANGUAGES[j], end=", ")
print()

def perform_clustering(matrix, data, iterations=100, hist=True):
  def print_clusters(clusters):
    for index, cluster in enumerate(clusters):
      for country in cluster:
        print(LANGUAGES[data[country]])
      print()

  kmc = KMemoidsClustering(matrix)
  clustering = [kmc.clustering(5) for _ in range(iterations)]

  silhouette_costs = [kmc.silhouette_cost(cluster[1])
                      for cluster in clustering]
  worst = min(zip(clustering, silhouette_costs), key=lambda x: x[1])
  best = max(zip(clustering, silhouette_costs), key=lambda x: x[1])

  print("Worst:")
  print_clusters(worst[0][1])
  print("Best:")
  print_clusters(best[0][1])

  if hist:
    pyplot.hist(silhouette_costs)
    pyplot.show()

# 1. in 2. nal
matrix = Matrix(3, file_names, preproccess_enabled=True)\
  .generate_distance_matrix()
perform_clustering(matrix, all_languages)

# 3. nal
class LanguagePredictor:
  def __init__(self, languages):
    self.languages = languages

  def predict_language(self, article):
    distances = Matrix(3, [article] + self.languages)\
      .generate_distance_matrix()[0][1:]
    distances = list(enumerate(distances))
    distances = sorted(distances, key=lambda x: x[1])
    most_probable = distances[:3]
    most_probable_sums = sum(1 - distance
                             for _, distance in most_probable)
    return [(index, (1 - distance) / most_probable_sums)
            for index, distance in most_probable]

import os

articles = os.listdir("articles")
articles = ["articles/"+article for article in articles]

print()
for article in articles:
  print("Prediting", article, ":")
  prediction = LanguagePredictor(file_names)\
    .predict_language(article)
  with open(article) as handle: # ad-hoc solution for displaying text
    print(handle.read()[:50])
  print()
  for country, percentage in prediction:
    print(LANGUAGES[all_languages[country]],
          "{0:.0f}%".format(percentage * 100))
  print()

# 1. dodatna
from hc import HierarchicalClustering

def cosine_dist(x, y):
  return 1 - (np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y)))

data = dict()
for index, file in enumerate(all_languages):
  data[file] = matrix[index]
hc = HierarchicalClustering(data, distance_calculation_strategy=cosine_dist)
hc.run()
hc.plot_tree()

# 2. dodatna
countries = [(article.split("_")[0]).split("/")[1] for article in articles]
matrix = Matrix(3, articles).generate_distance_matrix()
perform_clustering(matrix, countries, hist=False)