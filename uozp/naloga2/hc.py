from functools import reduce
from itertools import combinations

class HierarchicalClustering:
  def __init__(self, data, labels=None, distance_calculation_strategy=None):
    """Initialize the clustering"""
    self.data = data
    # self.clusters stores current clustering. It starts as a list of lists
    # of single elements, but then evolves into clusterings of the type
    # [[["Albert"], [["Branka"], ["Cene"]]], [["Nika"], ["Polona"]]]
    self.clusters = [[name] for name in self.data.keys()]
    self.distance_strategy = distance_calculation_strategy
    self.labels = labels

  def row_distance(self, key_a, key_b):
    """
    Distance between two rows.
    Implement either Euclidean or Manhattan distance.
    Example call: self.row_distance("Polona", "Rajko")
    """
    if key_a not in self.data or key_b not in self.data:
      raise Exception("Unknown keys.")

    vector_a = self.data[key_a]
    vector_b = self.data[key_b]
    return self.distance_strategy(vector_a, vector_b)

  def cluster_distance(self, cluster_a, cluster_b, strategy="average"):
    """
    Compute distance between two clusters.
    Implement either single, complete, or average linkage.
    Example call: self.cluster_distance(
        [[["Albert"], ["Branka"]], ["Cene"]],
        [["Nika"], ["Polona"]])
    :param strategy: one of: "ward", "average", "single", "complete"
    """

    def ward_distance(cluster_a, cluster_b):
      """
      Calculate `Ward` distance between two clusters.
      """
      def center_point(cluster):
        if type(cluster) == list:
          vectors = [center_point(sub_cluster) for sub_cluster in cluster]
          average = reduce(lambda acc, vec: avg_vectors(acc, vec), vectors, vectors[0])
          return average
        return self.data[cluster]

      center_a = center_point(cluster_a)
      center_b = center_point(cluster_b)
      return self.distance_strategy(center_a, center_b)

    def average_distance(cluster_a, cluster_b):
      """
      Calculate `average linkage` distance between two clusters.
      """
      def calculate_average_distance(cluster_a, cluster_b):
        distances_sum = sum(self.distance_strategy(self.data[a], self.data[b]) for a in cluster_a for b in cluster_b)
        return distances_sum / (len(cluster_a) * len(cluster_b))

      cluster_a = self._flat_map(cluster_a)
      cluster_b = self._flat_map(cluster_b)
      return calculate_average_distance(cluster_a, cluster_b)

    def single_linkage_distance(cluster_a, cluster_b):
      """
      Calculate `single linkage` distance between two clusters.
      """
      def calculate_distance(cluster_a, cluster_b):
        minimum_distance = min(self.distance_strategy(self.data[a], self.data[b]) for a in cluster_a for b in cluster_b)
        return minimum_distance

      cluster_a = self._flat_map(cluster_a)
      cluster_b = self._flat_map(cluster_b)
      return calculate_distance(cluster_a, cluster_b)

    def complete_linkage_distance(cluster_a, cluster_b):
      """
      Calculate `complete linkage` distance between two clusters.
      """
      def calculate_distance(cluster_a, cluster_b):
        maximum_distance = max(self.distance_strategy(self.data[a], self.data[b]) for a in cluster_a for b in cluster_b)
        return maximum_distance

      cluster_a = self._flat_map(cluster_a)
      cluster_b = self._flat_map(cluster_b)
      return calculate_distance(cluster_a, cluster_b)

    if strategy == "ward":
      strategy = ward_distance
    elif strategy == "single":
      strategy = single_linkage_distance
    elif strategy == "complete":
      strategy = complete_linkage_distance
    elif strategy == "average":
      strategy = average_distance
    else:
      raise Exception("Unknown strategy!")

    return strategy(cluster_a, cluster_b)

  def closest_clusters(self):
    """
    Find a pair of closest clusters and returns the pair of clusters and
    their distance.

    Example call: self.closest_clusters(self.clusters)
    """
    assert len(self.clusters) > 0

    dis, pair = min((self.cluster_distance(cluster_a, cluster_b), [cluster_a, cluster_b])
                    for cluster_a, cluster_b in combinations(self.clusters, 2))
    return pair, dis

  def run(self):
    """
    Given the data in self.data, performs hierarchical clustering.
    Can use a while loop, iteratively modify self.clusters and store
    information on which clusters were merged and what was the distance.
    Store this later information into a suitable structure to be used
    for plotting of the hierarchical clustering.
    """
    while 1:
      closest_clusters = self.closest_clusters()
      self.clusters.remove(closest_clusters[0][0])
      self.clusters.remove(closest_clusters[0][1])
      if len(self.clusters) == 0:
        self.clusters = closest_clusters[0]
        return
      self.clusters.append(closest_clusters[0])

  def plot_tree(self):
    """
    Use cluster information to plot an ASCII representation of the cluster
    tree.
    """
    def plot(cluster, indent=0):
      if type(cluster) == list and len(cluster) > 1:
          plot(cluster[0], indent + 4)
          print(" "*indent, "----|")
          plot(cluster[1], indent + 4)
      else:
        print(" "*indent, "----", cluster[0])

    plot(self.clusters)

  def _flat_map(self, cluster):
    """
    Flat map a recursive list of clusters into a list of points.
    """

    def internal_flat_map(cluster):
      if type(cluster) == list:
        for sub_cluster in cluster:
          internal_flat_map(sub_cluster)
      else:
        flat.append(cluster)

    flat = []
    internal_flat_map(cluster)
    return flat
