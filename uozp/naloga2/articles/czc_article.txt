K výročí republiky napadl sníh a přišly mrazíky. Zítra vyletí teploty až na 20 °C

Zatímco v pondělí má být až 20 stupňů, sváteční neděli doprovází déšť a místy dokonce sněhové přeháňky. Je to důsledek zvlněného frontálního rozhraní, které nad střední Evropou odděluje dvě teplotně rozdílné hmoty vzduchu. Počasí tak komplikuje oslavy 100 let od založení československé republiky.

Na Šumavě v noci na dnešek teploty klesly až tři stupně pod nulu a sněžilo, na nejvyšších vrcholech leželo přes deset centimetrů sněhu. Na Churáňově napadlo asi 12 centimetrů sněhu a na hoře Plechý až 13 centimetrů. Sněhová pokrývka dlouho nevydrží, během dne teploty stoupají. Ve večerních hodinách bude zřejmě pršet i v nejvyšších polohách Šumavy, řekl Gustav Sysel z českobudějovické pobočky Českého hydrometeorologického ústavu.

Na jihu Čech jde o první větší sněhovou pokrývku blížící se zimy. „Během noci se ochladilo a dešťové srážky přecházely v polohách nad 800 metrů ve sněhové,“ uvedl Sysel. V místech od 800 do 1000 metrů spadlo i devět centimetrů sněhu, mohly by se ještě objevit mrznoucí srážky. V nižších částech Jihočeského kraje slabě prší nebo mrholí, teploty se pohybují do pěti stupňů Celsia.

„V příštích dnech se naopak oteplí a bude spíš nadprůměrně teplé počasí,“ dodal Sysel. V noci na pondělí bude podle předpovědi meteorologů zataženo, místy s mlhami a ojediněle se slabým deštěm. Teploty se v pondělí přes den budou pohybovat mezi 12 až 16 stupni a ve východní polovině Česka se očekává až 20 stupňů Celsia.