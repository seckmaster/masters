Zbog ljubavi se nekadašnja manekenka Linardić iz Rijeke preselila u Zagreb, a četiri godine je u vezi s vaterpolistom Ivanom Capanom s kojim planira proširiti obitelj...

Pročitajte više na: https://www.24sata.hr/show/bivsa-corlukina-cura-je-teta-u-vrticu-djeca-joj-draza-od-piste-597202 - 24sata.hr

Riječanka Vedrana Linardić (33) prije deset godina bila je proglašena najseksi Hrvaticom, a nije bilo modne revije na kojoj se ona nije prošetala. Sada već dvije godine, kako kaže, radi posao iz snova. Vedrana je teta u zagrebačkom vrtiću, piše RTL.

- Ovdje se osjećam kao riba u vodi. Baš sam se pronašla i nije mi nimalo teško. Uživam i gušt mi je raditi ovaj posao s djecom - rekla je Linardić.

Riječanka je završila Učiteljsku akademiju jer ju je srce, kako tvrdi, vuklo prema radu s djecom. Zbog manekenske karijere i atraktivnog izgleda, često se susretala s predrasudama kako javne osobe ne mogu raditi taj posao.

Vedrana se zbog ljubavi iz Rijeke preselila u Zagreb, a četiri godine je u vezi s vaterpolistom Ivanom Capanom s kojim planira proširiti obitelj. Često ju se može vidjeti kako Ivana bodri na utakmicama.

- Držimo fige da se želje ostvare, a rad u vrtiću svakako je odlična priprema za ovaj najljepši posao u životu koji je tek čeka - za RTL je rekla Vedranina majka.

Riječanka je 2011. bila u kratkoj vezi s nogometašem Vedranom Ćorlukom (32), koji je danas u braku s Frankom Batelić (26) s kojom se, nakon Svjetskog prvenstva u Rusiji vjenčao u Istri.