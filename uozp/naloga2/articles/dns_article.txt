Skær fedtet af valgflæsket
Politikerne taler ned til borgerne, når de lancerer udspil til otte mia. kroner, der viser sig kun at være 800 mio. værd.

Det er ikke underligt, at politikerleden stiger, og tilliden falder, når politikerne tages i at pumpe vidtløftige løfter op til noget, de slet ikke kan bære.

Som nu vores sundhedsminister Ellen Trane Nørby (V), der i begyndelsen af oktober fremlagde forslag om, at der kunne spares otte mia. kroner i det offentlige sundhedsvæsen på såkaldte overflødige indlæggelser.

Pengene forsvandt ifølge ministeren ned i en Bermuda-trekant, hvor ingen anede, hvor de blev af, fordi de forskellige dele af sundhedsvæsenet er for dårlige til at tale sammen.

På Berlingske er vi bestemt tilhængere af at bruge skattekronerne mest fornuftigt, så hvis otte mia. kroner bruges overflødigt, er det da med at gøre noget ved det så hurtigt som muligt. Men det lød næsten for godt til at være sandt. Og det viste det sig også at være.

Berlingske har gennemgået tallene og blot 20 dage efter fremlæggelsen af forslaget, erkender sundhedsministeren, at en mere realistisk besparelse er 800 mio. kroner.

20 dage. Fra otte mia. kroner til 800 mio. Vi lader den lige stå et øjeblik.

Det er jo grænsende til latterliggørelse af befolkningen at kaste om sig med tal, som åbenbart har så lidt bund i virkeligheden. Dertil bør politikerne holde sig for gode og sætte hensynet til faglighed og ordentlighed højere end en hurtig omgang valgflæsk.

De kommende måneder må vi forvente, at der kommer til at fyge med den ene milliardsatsning efter den anden. Det er blevet sæson for valgløfter, og politikerne vil gerne tiltrække vælgernes opmærksomhed. Men hvis de også vil vinde vælgernes tillid, må metoden være en anden.

Det er ikke kun hos regeringen, der er grund til at se regnestykkerne efter i sømmene.

Socialdemokraterne lancerede for nylig en kampagne, hvor de beskyldte Venstre for at være skyld i færre sygeplejersker.https://www.berlingske.dk/nyheder/socialdemokratiet-kritiseres-for-skoed...)"> Kiggede man nærmere på Socialdemokraternes grafer kunne man se, at grafen ikke som normalt begynder ved nul men ved 35.000, hvilket betød, at socialdemokraternes budskab så mere slagkraftigt ud, end det reelt var.

En fiks manøvre, som fik sundhedsøkonom Kjeld Møller Pedersen til at konkludere, at der var tale om en »noget skødesløs omgang med statistikkerne«.

I overbudafdelingen findes også forslag, som ingen faktisk har brug for. Således præsenterede Socialdemokraterne tidligere på måneden forslaget om, at alle kvinder skulle have lov til blive på hospitalet to døgn efter, de har født.  Men hverken læger eller sundhedsøkonomer kunne se det fornuftige i forslaget. Og kvinderne; ja de ville helst bare hjem, så snart de var klar til det. Et overbud som ingen har brug for, og som ingen faglig begrundelse har, er spild af borgernes skattekroner.

Så kære politikere. Lad være med at tale ned til vælgerne ved at bruge oppustede tal og falske præmisser. Det risikerer at ramme som en boomerang, når de forlorne løfter bliver afsløret. Og heldigvis har vi en fri, ansvarlig presse, der ser det som sin fornemmeste opgave at løfte sløret for den slags.

Godt nok er det valgår, men politikerne burde tage den tillidskrise, de har med vælgerne, mere alvorligt og skære fedtet af valgflæsket.

METTE ØSTERGAARD