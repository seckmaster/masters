from collections import defaultdict
import numpy as np
import itertools
import pickle
import csv
import os
import math

class RecommenderSystem:
  def __init__(self, matrix):
    self.matrix = matrix

  def predict_all(self, dataset):
    for user_id, artist_id in dataset:
      prediction = self.predict(int(user_id), int(artist_id))
      yield prediction

  def predict(self, user_id, artist_id):
    matrix = self.matrix.matrix
    similarity_matrix = self.matrix.similarity_matrix

    if user_id not in self.matrix.user_id_mapping and artist_id not in self.matrix.artist_id_mapping:
      return 0

    if user_id not in self.matrix.user_id_mapping:
      return np.nanmean(matrix[:, self.matrix.artist_id_mapping[artist_id]])
    if artist_id not in self.matrix.artist_id_mapping:
      return np.nanmean(matrix[self.matrix.user_id_mapping[user_id], :])
    return self._predict(user_id,
                         artist_id,
                         self.matrix.similar_users(artist_id),
                         matrix,
                         similarity_matrix)

  def _predict(self, user_id, artist_id, similar_users, matrix, similarity_matrix):
    user = self.matrix.user_id_mapping[user_id]
    artist = self.matrix.artist_id_mapping[artist_id]

    user_average = np.nanmean(matrix[user, :])
    x = np.nansum(np.fromiter((similarity_matrix[user][other_user] * matrix[other_user][artist]
                               for other_user in similar_users), dtype=np.float64))
    if x == 0:
      return user_average

    y = np.nansum(np.fromiter((similarity_matrix[user][other_user]
                               for other_user in similar_users), dtype=np.float64))

    return (x / y) + user_average

  def _predict_empty_profile(self):
    return 0


class Reader:
  def __init__(self, filename, split=None):
    def read(filename):
      with open(filename) as handle:
        reader = csv.reader(handle, delimiter="\t")
        _ = next(reader)
        return list(reader)
    data = read(filename)
    if not split:
      self.data = data
      self.test = []
    else:
      t = int(len(data) * split)
      np.random.shuffle(np.array(data))
      self.data = data[:t]
      self.test = data[t:]


class Score:
  def __init__(self, artist_id, score):
    self.artist_id = artist_id
    self.score = score

  def __str__(self):
    return "" + self.artist_id + ": " + self.score


class Matrix:
  def __init__(self, reader, cache_enabled=True):
    def build_dict(data):
      matrix = defaultdict(list)
      user_id_mapping = dict()
      users_count = 0
      artist_id_mapping = dict()
      artists_count = 0

      for row in data:
        user_id = int(row[0])
        artist_id = int(row[1])
        score = float(row[2])

        if user_id not in user_id_mapping:
          user_id_mapping[user_id] = users_count
          users_count += 1

        if artist_id not in artist_id_mapping:
          artist_id_mapping[artist_id] = artists_count
          artists_count += 1

        matrix[user_id].append(Score(artist_id, score))
      return matrix, user_id_mapping, artist_id_mapping

    def build_matrix(d):
      matrix = np.empty(shape=(len(d), 17632))
      matrix[:] = np.nan
      for user_id, scores in d.items():
        user = self.user_id_mapping[user_id]
        for score in scores:
          artist = self.artist_id_mapping[score.artist_id]
          matrix[user][artist] = score.score
      return matrix

    def build_similarity_matrix(matrix, cache_enabled, dump_file):
      try:
        if not cache_enabled:
          raise Exception()
        cached = pickle.load(open(dump_file, "rb"))
        # print(" .. from cache")
        return cached
      except Exception as err:
        def user_similarity(u1, u2):
          def intersect(u1, u2):
            masked = ~np.logical_or(np.isnan(u1), np.isnan(u2))
            return u1[masked], u2[masked]

          a, b = intersect(matrix[u1], matrix[u2])
          if np.size(a) == 0 or np.size(b) == 0:
            return 0
          similarity = np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))
          return similarity

        # print(" .. building matrix")
        similarity_matrix = np.zeros(shape=(len(matrix), len(matrix)))
        for u1, u2 in itertools.combinations(self.user_id_mapping.values(), 2):
          similarity = (1 + user_similarity(u1, u2)) * 0.5
          similarity_matrix[u1][u2] = similarity

        pickle.dump(similarity_matrix, open(dump_file, "wb"))
        return similarity_matrix

    d, u, a = build_dict(reader.data)
    self.user_id_mapping = u
    self.artist_id_mapping = a
    self.matrix = build_matrix(d)
    self.similarity_matrix = build_similarity_matrix(self.matrix,
                                                     cache_enabled,
                                                     "._cache")

  def __getitem__(self, user_id):
    user = self.user_id_mapping[user_id]
    return self.matrix[user]

  def similarity(self, user_a, user_b):
    u1 = self.user_id_mapping[user_a]
    u2 = self.user_id_mapping[user_b]
    return self.similarity_matrix[u1][u2]

  def similar_users(self, artist_id):
    user_ids = []
    for user_id in self.user_id_mapping.keys():
      user = self.user_id_mapping[user_id]
      artist = self.artist_id_mapping[artist_id]
      if self.matrix[user][artist] > 0:
        user_ids.append(user)
    return np.array(user_ids)

  def score(self, user_id, artist_id):
    user = self.user_id_mapping[user_id]
    artist = self.artist_id_mapping[artist_id]
    return self.matrix[user][artist]


class NMF:
  def __init__(self, rank=10, max_iter=100, eta=0.01):
    self.rank = rank
    self.max_iter = max_iter
    self.eta = eta
    self.W = None
    self.H = None

  def fit(self, X):
    m, n = X.shape

    W = np.random.rand(m, self.rank)
    H = np.random.rand(n, self.rank)
    H = H.T

    # Errors
    self.error = np.zeros((self.max_iter,))

    for t in range(self.max_iter):
      print("Iteration: " + str(t))
      for i in range(m):
        for j in range(n):
          if X[i][j] > 0:
            J = X[i][j] - np.dot(W[i, :], H[:, j])
            for k in range(self.rank):
              W[i][k] += self.eta * J * W[i][k]
              H[k][j] += self.eta * J * H[k][j]

      self.error[t] = np.linalg.norm((X - W.dot(H))[X > 0]) ** 2
      # print(t, self.error[t])

    self.W = W
    self.H = H.T
    return self.error[self.max_iter - 1]

  def predict(self, i, j):
    return self.W[i, :].dot(self.H[j, :])

  def predict_all(self):
    return self.W.dot(self.H.T)



def friends_list(reader):
  friends = defaultdict(set)
  for user_id, friend_id in reader.data:
    user = int(user_id)
    friend = int(friend_id)
    friends[user].add(friend)
    friends[friend].add(user)
  return friends


train = Reader("data/user_artists_training.dat")
test = Reader("data/user_artists_test.dat")

# print("... building matrix")
matrix = Matrix(train)
friends = friends_list(Reader("data/user_friends.dat"))


def save(predictions):
  # print("... writing to output file")
  ls = os.listdir("out/")
  if len(ls) == 0:
    count = 1
  else:
    count = max([int(i.split(".")[0].replace("prediction", "")) for i in ls]) + 1
  with open("out/prediction"+str(count)+".txt", "w") as handle:
    for pred in predictions:
      handle.write(str(pred))
      handle.write("\n")


def first():
  rec = RecommenderSystem(matrix)
  print("... predicting")
  predictions = list(rec.predict_all(test.data))
  print("... done")
  save(predictions)


def nmf_recommender(train, test, should_print=False):
  def friends_average(user_id, artist_id):
    if len(friends[user_id]) == 0 or artist_id not in matrix.artist_id_mapping:
      return None
    artist = matrix.artist_id_mapping[artist_id]
    s = sum(matrix.matrix[matrix.user_id_mapping[friend][artist]]
            for friend in friends[user_id])
    return s / len(friends[user_id])

  try:
    with open("nmf", "rb") as handle:
      # print("... from disk")
      nmf = pickle.load(handle)
      # print("... loaded")
  except Exception as err:
    print(err)
    print("... calculating nmf")
    nmf = NMF(rank=5, max_iter=100, eta=0.01)
    nmf.fit(train)
    print("... done")
    with open("nmf", "wb") as f:
      pickle.dump(nmf, f)

  mat = nmf.predict_all()
  predictions = []
  for row in test:
    user_id = int(row[0])
    artist_id = int(row[1])

    if user_id not in matrix.user_id_mapping:
      prediction = friends_average(user_id, artist_id)
      if prediction:
        predictions.append(prediction)
      else:
        predictions.append(np.nanmean(mat[:, matrix.artist_id_mapping[artist_id]]))
      # predictions.append(np.nanmean(matrix.matrix[:, matrix.artist_id_mapping[artist_id]]))
      continue
    if artist_id not in matrix.artist_id_mapping:
      prediction = friends_average(user_id, artist_id)
      if prediction:
        predictions.append(prediction)
      else:
        predictions.append(np.nanmean(mat[matrix.user_id_mapping[user_id], :]))
      # predictions.append(np.nanmean(matrix.matrix[matrix.user_id_mapping[user_id], :]))
      continue

    user = matrix.user_id_mapping[int(user_id)]
    artist = matrix.artist_id_mapping[int(artist_id)]
    prediction = nmf.predict(user, artist)
    predictions.append(prediction)

  # print("... saving predictions")
  # save(predictions)

  if should_print:
    for pred in predictions:
      print(pred)

  return predictions

def rmse_nmf():
  def error():
    reader = Reader("data/user_artists_training.dat", split=0.7)
    train, test = reader.data, reader.test
    predictions = nmf_recommender(train, test)
    me = sum((float(t[2]) - p) ** 2
             for t, p in zip(test, predictions)) / len(predictions)
    rmse = math.sqrt(me)
    return rmse
  print("nmf rmse: ", sum(error() for _ in range(5)) / 5)

def rmse_simcos():
  def error():
    reader = Reader("data/user_artists_training.dat", split=0.7)
    matrix = Matrix(reader, cache_enabled=False)
    predictor = RecommenderSystem(matrix)
    sum_errors = 0
    for user_id, artist_id, real in reader.test:
      p = predictor.predict(int(user_id), int(artist_id))
      sum_errors += (p - float(real)) ** 2
    me = sum_errors / len(reader.test)
    rmse = math.sqrt(me)
    return rmse
  print("simcos rmse: ", sum(error() for _ in range(5)) / 5)

# rmse_nmf()
# rmse_simcos()

nmf_recommender(train=matrix.matrix,
                test=test.data,
                should_print=True)

# print("... exiting")