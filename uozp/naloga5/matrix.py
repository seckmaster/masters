from collections import defaultdict
import numpy as np
import itertools
import pickle


class Score:
  def __init__(self, artist_id, score):
    self.artist_id = artist_id
    self.score = score

  def __str__(self):
    return "" + self.artist_id + ": " + self.score


class Matrix:
  def __init__(self, reader):
    def build_dict(data):
      matrix = defaultdict(list)
      user_id_mapping = dict()
      users_count = 0
      artist_id_mapping = dict()
      artists_count = 0

      for row in data:
        user_id = int(row[0])
        artist_id = int(row[1])
        score = float(row[2])

        if user_id not in user_id_mapping:
          user_id_mapping[user_id] = users_count
          users_count += 1

        if artist_id not in artist_id_mapping:
          artist_id_mapping[artist_id] = artists_count
          artists_count += 1

        matrix[user_id].append(Score(artist_id, score))
      return matrix, user_id_mapping, artist_id_mapping

    def build_matrix(d):
      matrix = np.empty(shape=(len(d), 17632))
      matrix[:] = np.nan
      for user_id, scores in d.items():
        user = self.user_id_mapping[user_id]
        for score in scores:
          artist = self.artist_id_mapping[score.artist_id]
          matrix[user][artist] = score.score
      return matrix

    def build_average_matrix():
      matrix = np.empty(shape=(len(d), 17632))
      matrix[:] = np.nan
      for i in range(len(self.matrix)):
        mean = np.nanmean(self.matrix[i, :])
        matrix[i, :] = np.subtract(self.matrix[i, :], mean)
      return matrix

    def build_similarity_matrix(matrix, dump_file):
      try:
        cached = pickle.load(open(dump_file, "rb"))
        return cached
      except:
        def user_similarity(u1, u2):
          def intersect(u1, u2):
            masked = ~np.logical_or(np.isnan(u1), np.isnan(u2))
            return u1[masked], u2[masked]

          a, b = intersect(matrix[u1], matrix[u2])
          if np.size(a) == 0 or np.size(b) == 0:
            return 0
          similarity = np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))
          return similarity

        similarity_matrix = np.zeros(shape=(len(matrix), len(matrix)))
        for u1, u2 in itertools.combinations(self.user_id_mapping.values(), 2):
          similarity = (1 + user_similarity(u1, u2)) * 0.5
          similarity_matrix[u1][u2] = similarity

        pickle.dump(similarity_matrix, open(dump_file, "wb"))
        return similarity_matrix

    d, u, a = build_dict(reader.data)
    self.user_id_mapping = u
    self.artist_id_mapping = a
    self.matrix = build_matrix(d)
    self.average_matrix = build_average_matrix()
    self.similarity_matrix = build_similarity_matrix(self.matrix, "._cache")
    self.average_similarity_matrix = build_similarity_matrix(self.average_matrix, "._cache2")

  def __getitem__(self, user_id):
    user = self.user_id_mapping[user_id]
    return self.matrix[user]

  def similarity(self, user_a, user_b):
    u1 = self.user_id_mapping[user_a]
    u2 = self.user_id_mapping[user_b]
    return self.similarity_matrix[u1][u2]

  def similar_users(self, artist_id):
    user_ids = []
    for user_id in self.user_id_mapping.keys():
      user = self.user_id_mapping[user_id]
      artist = self.artist_id_mapping[artist_id]
      if self.matrix[user][artist] > 0:
        user_ids.append(user)
    return np.array(user_ids)

  def score(self, user_id, artist_id):
    user = self.user_id_mapping[user_id]
    artist = self.artist_id_mapping[artist_id]
    return self.matrix[user][artist]