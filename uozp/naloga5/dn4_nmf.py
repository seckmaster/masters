import numpy as np


class NMF:
  """
  Fit a matrix factorization model for a matrix X with missing values.
  such that
      X = W H.T + E
  where
      X is of shape (m, n)    - data matrix
      W is of shape (m, rank) - approximated row space
      H is of shape (n, rank) - approximated column space
      E is of shape (m, n)    - residual (error) matrix
  """

  def __init__(self, rank=10, max_iter=100, eta=0.01):
    """
    :param rank: Rank of the matrices of the model.
    :param max_iter: Maximum nuber of SGD iterations.
    :param eta: SGD learning rate.
    """
    self.rank = rank
    self.max_iter = max_iter
    self.eta = eta
    self.W = None
    self.H = None

  def fit(self, X):
    """
    Fit model parameters W, H.
    :param X:
        Non-negative data matrix of shape (m, n)
        Unknown values are assumed to take the value of zero (0).
    """
    m, n = X.shape

    W = np.random.rand(m, self.rank)
    H = np.random.rand(n, self.rank)
    H = H.T

    # Errors
    self.error = np.zeros((self.max_iter,))

    for t in range(self.max_iter):
      print("Iteration: " + str(t))
      for i in range(m):
        for j in range(n):
          if X[i][j] > 0:
            J = X[i][j] - np.dot(W[i, :], H[:, j])
            for k in range(self.rank):
              W[i][k] += self.eta * J * W[i][k]
              H[k][j] += self.eta * J * H[k][j]

      self.error[t] = np.linalg.norm((X - W.dot(H))[X > 0]) ** 2
      # print(t, self.error[t])

    self.W = W
    self.H = H.T
    return self.error[self.max_iter - 1]

  def predict(self, i, j):
    """
    Predict score for row i and column j
    :param i: Row index.
    :param j: Column index.
    """
    return self.W[i, :].dot(self.H[j, :])

  def predict_all(self):
    """
    Return approximated matrix for all
    columns and rows.
    """
    return self.W.dot(self.H.T)
