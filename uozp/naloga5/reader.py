import csv


class Reader:
  def __init__(self, filename):
    def read(filename):
      with open(filename) as handle:
        reader = csv.reader(handle, delimiter="\t")
        _ = next(reader)
        return list(reader)
    self.data = read(filename)
