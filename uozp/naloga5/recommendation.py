import numpy as np


class RecommenderSystem:
  def __init__(self, matrix):
    self.matrix = matrix

  def predict_all(self, dataset):
    for user_id, artist_id in dataset:
      prediction = self.predict(int(user_id), int(artist_id))
      yield prediction

  def predict(self, user_id, artist_id):
    matrix = self.matrix.matrix
    similarity_matrix = self.matrix.similarity_matrix

    if user_id not in self.matrix.user_id_mapping:
      return np.nanmean(matrix[:, self.matrix.artist_id_mapping[artist_id]])
    if artist_id not in self.matrix.artist_id_mapping:
      return np.nanmean(matrix[self.matrix.user_id_mapping[user_id], :])
    return self._predict(user_id,
                         artist_id,
                         self.matrix.similar_users(artist_id),
                         matrix,
                         similarity_matrix)

  def _predict(self, user_id, artist_id, similar_users, matrix, similarity_matrix):
    user = self.matrix.user_id_mapping[user_id]
    artist = self.matrix.artist_id_mapping[artist_id]

    user_average = np.nanmean(matrix[user, :])
    x = np.nansum(np.fromiter((similarity_matrix[user][other_user] * matrix[other_user][artist]
                               for other_user in similar_users), dtype=np.float64))
    if x == 0:
      return user_average

    y = np.nansum(np.fromiter((similarity_matrix[user][other_user]
                               for other_user in similar_users), dtype=np.float64))

    return (x / y) + user_average

  def _predict_empty_profile(self):
    return 0
