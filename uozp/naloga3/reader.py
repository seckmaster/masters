import csv
from collections import defaultdict
from datetime import datetime

class Data:
  def __init__(self, train, test):
    self.train = train
    self.test = test

  def __call__(self, *args, **kwargs):
    return self.train, self.test

class Lines:
  def __init__(self, metadata, lines):
    self.metadata = metadata
    self.lines = lines

  def __getitem__(self, item):
    return self.lines[item]

  def __call__(self, *args, **kwargs):
    indices = [self.metadata.index(i) for i in kwargs["columns"]]

    if "line_number" in kwargs:
      lines = self.lines[kwargs["line_number"]]
    else:
      lines = self.lines

    rows = []
    for row in lines:
      new_row = [None for _ in range(len(indices))]
      for i, index in enumerate(indices):
        new_row[i] = row[index]
      rows.append(new_row)
    return rows

class Preprocessor:
  DATE_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
  HOLIDAYS = {"2012-01-01", "2012-01-02", "2012-02-08", "2012-04-08", "2012-04-09", "2012-04-27", "2012-05-01",
              "2012-05-02", "2012-06-25", "2012-08-15", "2012-10-31", "2012-11-01", "2012-12-25", "2012-12-26"}
  ADDITIONAL_HEADERS = ["Direction",
                        "Departure epoch",
                        "Departure seconds",
                        "Departure minutes",
                        "Departure hour",
                        "Departure day&hour",
                        "Departure day",
                        "Departure weekday",
                        "Departure month",
                        "Precipitation",
                        "Is holiday",
                        "Is weekend",
                        "Arrival epoch",
                        "Arrival seconds",
                        "Arrival minutes",
                        "Arrival hour",
                        "Arrival day",
                        "Arrival month",
                        "Delta time"]

  def __init__(self, reader):
    self.metadata = reader.train_data[1]
    self.data = self._preprocess(reader)

  def _preprocess(self, reader):
    def is_holiday(date):
      return int(date[:10] in Preprocessor.HOLIDAYS)
    def is_weekend(day):
      return int(day >= 5 and day <= 7)

    def preprocess_row(row):
      updated_row = row[:]
      updated_row[1] = int(row[1])
      updated_row[2] = int(row[2])
      updated_row.append(0)

      # timestamps
      departure_time = datetime.strptime(row[6],
                                         Preprocessor.DATE_FORMAT)
      updated_row.append(departure_time.timestamp())
      updated_row.append(departure_time.second)
      updated_row.append(departure_time.minute)
      updated_row.append(departure_time.hour)
      updated_row.append(departure_time.day*24 + departure_time.hour)
      updated_row.append(departure_time.day)
      updated_row.append(departure_time.weekday())
      updated_row.append(departure_time.month)
      updated_row.append(reader.weather[row[6]])
      updated_row.append(is_holiday(row[6]))
      updated_row.append(is_weekend(departure_time.day))
      if row[8] != "?":
        arrival_time = datetime.strptime(row[8],
                                         Preprocessor.DATE_FORMAT)
        updated_row.append(arrival_time.timestamp())
        updated_row.append(arrival_time.second)
        updated_row.append(arrival_time.minute)
        updated_row.append(arrival_time.hour)
        updated_row.append(arrival_time.day)
        updated_row.append(arrival_time.month)
        updated_row.append(arrival_time.timestamp() - departure_time.timestamp())
      return updated_row

    def preprocess(data):
      lines = defaultdict(list)
      for line_number, rows in data[0].items():
        for row in rows:
          preprocessed = preprocess_row(row)
          lines[line_number].append(preprocessed)
      return lines

    train = preprocess(reader.train_data)
    test = [preprocess_row(row) for row in reader.test_data]
    train_metadata = self.metadata + Preprocessor.ADDITIONAL_HEADERS
    return Data(Lines(train_metadata, train),
                Lines(train_metadata, test))

class Reader:
  def __init__(self, train, test, weather):
    self.weather = self._parse_weather(weather)
    self.test_data = self._parse_test(test)
    self.train_data = self._parse_train(train)

  def _parse_train(self, file):
    def id_from_row(row):
      return row[2] + row[3]

    with open(file) as handle:
      reader = csv.reader(handle, delimiter="\t")
      metadata = next(reader)

      lines = defaultdict(list)
      for row in reader:
        lines[id_from_row(row)].append(row)

      result = defaultdict(list)
      for line_number, item in lines.items():
        result[line_number] = item
      return result, metadata

  def _parse_test(self, file):
    with open(file) as handle:
      reader = csv.reader(handle, delimiter="\t")
      _ = next(reader)
      return list(reader)

  def _parse_weather(self, file):
    with open(file) as handle:
      reader = csv.reader(handle, delimiter=",")
      _ = next(reader)
      weather = defaultdict(float)
      for row in reader:
        if len(row) == 0: continue
        date = row[2].split(" ")[0]
        mm = row[3]
        weather[date] += float(mm)
      return weather