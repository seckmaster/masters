from sklearn.model_selection import train_test_split
import linear

class Model:
  def __init__(self, X, y, err="mae"):
    X_train, X_test, y_train, y_test = self._split_dataset(X, y)
    self.model = self._build_model(X_train, y_train)
    self.error = self._test_model(X_test, y_test, err)

  def _split_dataset(self, X, y):
    return train_test_split(X, y, test_size=0.05)

  def _build_model(self, X, y, lambda_=0):
    lr = linear.LinearLearner(lambda_=lambda_)
    return lr(X, y)

  def _test_model(self, X_test, y_test, err):
    def squared_err(pred, real):
      return (pred - real) ** 2

    def abs_err(pred, real):
      return abs(pred - real)

    err_fn = {"mse": squared_err, "mae": abs_err}[err]

    if len(X_test) == 0: return 0

    sum_err = 0
    for x, real in zip(X_test, y_test):
      pred = self.model(x)
      error = err_fn(pred, real)
      sum_err += error

    return sum_err / len(X_test)

  def predict(self, x):
    return self.model(x)