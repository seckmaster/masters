from reader import Reader, Preprocessor
from model import Model
from datetime import datetime
import math
import numpy as np
import pickle

def load_data(from_cache):
  try:
    if not from_cache: raise Exception()
    print("Attemting to load data from cache ...")
    pred_data = pickle.load(open("._cache_pred", "rb"))
    data = pickle.load(open("._cache", "rb"))
    print("Done ... ")
  except:
    print("Cache not found, parsing data ...")
    reader = Reader("data/train.csv",
                    "data/test.csv",
                    "data/weather.csv")
    preprocessor = Preprocessor(reader)
    data = preprocessor.data
    print("Storing data in cache ...")
    pickle.dump(data, open("._cache", "wb"))
    reader = Reader("data/train_pred.csv",
                    "data/test_pred.csv",
                    "data/weather.csv")
    preprocessor = Preprocessor(reader)
    pred_data = preprocessor.data
    print("Storing pred data in cache ...")
    pickle.dump(pred_data, open("._cache_pred", "wb"))
    print("Done ... ")

  train, test = data()
  train_pred, test_pred = pred_data()
  return (train, test), (train_pred, test_pred)

def initiate_learning(train, test, output_file="data/output2", one_hot_encoding=True):
  def one_hot_encode(value, max_val):
    encoded = np.zeros(max_val)
    encoded[value] = 1
    return encoded.tolist()

  def encode_rows(rows):
    encoded_rows = []
    for row in rows:
      month = one_hot_encode(row[0] - 1, 12)
      month = []
      hour = one_hot_encode(row[1] - 1, 24)
      # hour = []
      weekday = one_hot_encode(row[2] - 1, 7)
      # weekday = []
      day = one_hot_encode(row[3] - 1, 31)
      day = []
      minutes = one_hot_encode(row[4], 60)
      # minutes = []
      seconds = one_hot_encode(row[5], 60)
      seconds = []
      # minutes_seconds = one_hot_encode(math.floor(row[4] * 60 + row[5]), 3600)
      precipitation = one_hot_encode(math.floor(row[6]), 56)
      # precipitation = []
      driver_id = one_hot_encode(min(row[7] - 1, 429), 430)
      driver_id = []
      other = row[8:]
      # other = []
      encoded_rows.append(month + hour + weekday + day + minutes + seconds + precipitation + driver_id + other)
    return encoded_rows

  print("Generating", len(train.lines), "models ...")
  models = {}
  for line_number in train.lines:
    rows = train(line_number=line_number,
                 columns=columns)
    if one_hot_encoding:
      rows = encode_rows(rows)
    X = np.array(rows)
    y = np.array(train(line_number=line_number,
                       columns=target_column)).flatten().T
    model = Model(X, y)
    models[line_number] = model

  print("Finished generating models ...")
  print("MAE:", sum(model.error for model in models.values()) / len(models))
  print()
  print("Predictions:")

  metadata = test(columns=["Route",
                           "Route Direction",
                           "Departure time",
                           "Departure epoch",
                           ])
  rows = test(columns=columns)
  if one_hot_encoding:
    rows = encode_rows(rows)
  X = np.array(rows)

  delta_predictions = "Delta time" in target_column

  with open(output_file, "w") as handle:
    for metadata, x in zip(metadata, X):
      line_id = str(metadata[0]) + metadata[1]
      model = models[line_id]
      prediction = model.predict(x)
      if delta_predictions:
        prediction += metadata[3]
      time = str(datetime.fromtimestamp(prediction))
      handle.write(time + "\n")
      # print(x[2], time, prediction - x[3])

if __name__ == "__main__":
  columns = [
    "Departure month",
    "Departure hour",
    "Departure weekday",
    "Departure day",
    "Departure minutes",
    "Departure seconds",
    "Precipitation",
    "Driver ID",
    # ... next columns won't be hot_encoded ...
    "Is holiday",
    "Is weekend",
    # "Departure epoch",
  ]
  target_column = ["Delta time"]
  # target_column = ["Arrival epoch"]
  (train, test), (train_pred, test_pred) = load_data(from_cache=True)
  initiate_learning(train, test, one_hot_encoding=True)

