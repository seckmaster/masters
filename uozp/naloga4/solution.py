import numpy as np
from math import e
from scipy.optimize import fmin_l_bfgs_b

def load(name):
  """
  Odpri datoteko. Vrni matriko primerov (stolpci so znacilke)
  in vektor razredov.
  """
  data = np.loadtxt(name)
  X, y = data[:, :-1], data[:, -1].astype(np.int)
  return X, y


def h(x, theta):
  """
  Napovej verjetnost za razred 1 glede na podan primer (vektor vrednosti
  znacilk) in vektor napovednih koeficientov theta.
  """
  def probability_function(sum):
    return 1 / (1 + e ** -sum)

  weighted_sum = np.dot(x, theta)
  return probability_function(weighted_sum)


def cost(theta, X, y, lambda_):
  """
  Vrednost cenilne funkcije.
  """
  def cost(x, y):
    probability = h(x, theta)
    cost = (np.log(probability) * y) + (np.log(1 - probability) * (1 - y))
    return cost

  def reg_factor():
    return (lambda_ / (2 * len(X))) * np.dot(theta, theta)

  sum_cost = -sum(cost(x, y) for x, y in zip(X, y)) / len(X)
  return sum_cost + reg_factor()


def grad(theta, X, y, lambda_):
  """
  Odvod cenilne funkcije. Vrne numpyev vektor v velikosti vektorja theta.
  """
  def grad_cost(x, y):
    return y - h(x, theta)

  lambda_ = lambda_ / len(X)
  return np.array([(lambda_ * th) + (sum(grad_cost(x, y) * x[i] for x, y in zip(X, y)) / -len(X))
                   for i, th in enumerate(theta)])


def num_grad(theta, X, y, lambda_, e=1e-4):
  #   """
  #   Odvod cenilne funkcije izracunan numericno.
  #   Vrne numpyev vektor v velikosti vektorja theta.
  #   Za racunanje gradienta numericno uporabite funkcijo cost.
  def grad(eps):
    a = cost(theta + eps, X, y, lambda_)
    b = cost(theta - eps, X, y, lambda_)
    gradient = (a - b) / (2 * e)
    return gradient

  return np.array([grad(eps)
                   for eps in np.identity(len(theta)) * e])


class LogRegClassifier(object):
  def __init__(self, th):
    self.th = th

  def __call__(self, x):
    """
    Napovej razred za vektor vrednosti znacilk. Vrni
    seznam [ verjetnost_razreda_0, verjetnost_razreda_1 ].
    """
    x = np.hstack(([1.], x))
    p1 = h(x, self.th)  # verjetno razreda 1
    return [1 - p1, p1]


class LogRegLearner(object):
  def __init__(self, lambda_=0.0):
    self.lambda_ = lambda_

  def __call__(self, X, y):
    """
    Zgradi napovedni model za ucne podatke X z razredi y.
    """
    X = np.hstack((np.ones((len(X), 1)), X))

    # optimizacija
    theta = fmin_l_bfgs_b(
      cost,
      x0=np.zeros(X.shape[1]),
      args=(X, y, self.lambda_),
      fprime=grad)[0]

    return LogRegClassifier(theta)


def test_learning(learner, X, y):
  """ vrne napovedi za iste primere, kot so bili uporabljeni pri učenju.
  To je napačen način ocenjevanja uspešnosti!

  Primer klica:
      res = test_learning(LogRegLearner(lambda_=0.0), X, y)
  """
  c = learner(X, y)
  results = [c(x) for x in X]
  return results


def test_cv(learner, X, y, k=5):
  def divide(X, y, k):
    indices = list(range(len(X)))
    np.random.shuffle(indices)
    return indices

  def select(X, y, indices):
    return X[indices], y[indices]

  def split(X, y, test_indices):
    learn_indices = [i for i in range(len(indices)) if i not in test_indices]
    X_learn, y_learn = select(X, y, learn_indices)
    X_test, _ = select(X, y, test_indices)
    return X_learn, y_learn, X_test

  def classify(X_learn, y_learn, X_test):
    classifier = learner(X_learn, y_learn)
    return [classifier(x) for x in X_test]

  indices = divide(X, y, k)
  size = int(len(indices) / k)
  predictions = np.zeros(shape=(len(X), 2))
  for i in range(k):
    end = (i + 1) * size if i + 1 < k else max((i + 1) * size, len(X))
    test_indices = indices[i * size: end]
    predict = classify(*(split(X, y, test_indices)))
    for prediction, prediction_index in zip(predict, test_indices):
      predictions[prediction_index] = prediction
  return predictions


def CA(real, predictions):
  return sum(r == np.argmax(p, axis=0)
             for r, p in zip(real, predictions)) / len(real)


def AUC(real, predictions):
  def cartesian_product(x, y):
    # https://stackoverflow.com/a/11144716
    return np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])

  def area(neg_predictions, pos_predictions):
    def value(x, y):
      return 1 if x < y else 0 if x > y else 0.5

    product = cartesian_product(neg_predictions, pos_predictions)
    return np.mean([value(x, y) for x, y in product])

  neg_predictions = np.array([p[1] for r, p in zip(real, predictions) if r == 0])
  pos_predictions = np.array([p[1] for r, p in zip(real, predictions) if r == 1])
  return area(neg_predictions, pos_predictions)

if __name__ == "__main__":
  X, y = load('reg.data')

  learner = LogRegLearner(lambda_=0.0)
  classifier = learner(X, y)
  predictions = [classifier(x) for x in X]
  auc = AUC(y, predictions)
  cv = test_cv(learner, X, y)
  ca = CA(y, predictions)
  a = test_learning(learner, X, y)

  print("AUC:", auc)
  print("CA:", ca)
  print("CV:", cv)
  print("Learning:", a)
