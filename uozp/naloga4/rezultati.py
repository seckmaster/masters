import pylab
import numpy
from numpy.distutils.system_info import numarray_info
from solution import *

def draw_decision(X, y, classifier, at1, at2, grid=50):
  points = numpy.take(X, [at1, at2], axis=1)
  maxx, maxy = numpy.max(points, axis=0)
  minx, miny = numpy.min(points, axis=0)
  difx = maxx - minx
  dify = maxy - miny
  maxx += 0.02 * difx
  minx -= 0.02 * difx
  maxy += 0.02 * dify
  miny -= 0.02 * dify

  for c, (x, y) in zip(y, points):
    pylab.text(x, y, str(c), ha="center", va="center")
    pylab.scatter([x], [y], c=["b", "r"][c != 0], s=200)

  num = grid
  prob = numpy.zeros([num, num])
  for xi, x in enumerate(numpy.linspace(minx, maxx, num=num)):
    for yi, y in enumerate(numpy.linspace(miny, maxy, num=num)):
      # probability of the closest example
      diff = points - numpy.array([x, y])
      dists = (diff[:, 0] ** 2 + diff[:, 1] ** 2) ** 0.5  # euclidean
      ind = numpy.argsort(dists)
      prob[yi, xi] = classifier(X[ind[0]])[1]

  pylab.imshow(prob, extent=(minx, maxx, maxy, miny))

  pylab.xlim(minx, maxx)
  pylab.ylim(miny, maxy)
  pylab.xlabel(at1)
  pylab.ylabel(at2)

  pylab.show()

if __name__ == "__main__":
  X, y = load('reg.data')

  for lambda_ in [250]:
    learner = LogRegLearner(lambda_=lambda_)
    classifier = learner(X, y)
    draw_decision(X, y, classifier, 0, 1)

  lambdas = [0.0, 0.5, 1.5, 3, 5, 10, 12, 20, 50, 100]

  for lambda_ in lambdas:

    learner = LogRegLearner(lambda_=lambda_)
    classifier = learner(X, y)
    predictions = [classifier(x) for x in X]
    auc = AUC(y, predictions)
    cv = test_cv(learner, X, y)
    ca = CA(y, predictions)
    a = test_learning(learner, X, y)

    print("Lambda:", lambda_)
    print("AUC:", auc)
    print("CA:", ca)
    print("CV:", CA(cv, y))
    print("Learning:", CA(a, y))
    print()