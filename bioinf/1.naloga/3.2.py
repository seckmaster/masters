def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

class Fasta:
  def __init__(self, file):
    self.file = file
    # self.__generator = self.__parser()
    self.keys = None
    self.values = None

  def __iter__(self):
    if self.keys:
      for key, value in zip(self.keys, self.values):
        yield key, value
    else:
      self.__parse_iterator()

  def __parse_iterator(self):
    with open(self.file) as handle:
      value = ""
      for line in handle.readlines():
        if line.startswith(">"):
          key = line[1:-1]
          if value:
            yield key, value
            value = ""
        else:
          value += line[:-1]
      else:
        yield key, value


for key, value in Fasta("dataset"):
  print(key, value)

exit()

class Seq(str):
  codon_table = {
    "UUU": "F", "CUU": "L", "AUU": "I", "GUU": "V",
    "UUC": "F", "CUC": "L", "AUC": "I", "GUC": "V",
    "UUA": "L", "CUA": "L", "AUA": "I", "GUA": "V",
    "UUG": "L", "CUG": "L", "AUG": "M", "GUG": "V",
    "UCU": "S", "CCU": "P", "ACU": "T", "GCU": "A",
    "UCC": "S", "CCC": "P", "ACC": "T", "GCC": "A",
    "UCA": "S", "CCA": "P", "ACA": "T", "GCA": "A",
    "UCG": "S", "CCG": "P", "ACG": "T", "GCG": "A",
    "UAU": "Y", "CAU": "H", "AAU": "N", "GAU": "D",
    "UAC": "Y", "CAC": "H", "AAC": "N", "GAC": "D",
    "UAA": "Stop", "CAA": "Q", "AAA": "K", "GAA": "E",
    "UAG": "Stop", "CAG": "Q", "AAG": "K", "GAG": "E",
    "UGU": "C", "CGU": "R", "AGU": "S", "GGU": "G",
    "UGC": "C", "CGC": "R", "AGC": "S", "GGC": "G",
    "UGA": "Stop", "CGA": "R", "AGA": "R", "GGA": "G",
    "UGG": "W", "CGG": "R", "AGG": "R", "GGG": "G",
  }

  stop_codons = {key for key, value in codon_table.items() if value == "Stop"}
  start_codons = {key for key, value in codon_table.items() if value == "M"}

  def to_protein(self, to_stop=True):
    protein = ""
    for codon in self.__generate_tuples():
      if codon in Seq.stop_codons: break
      protein += Seq.codon_table[codon]
    else:
      return None if to_stop else protein
    return protein

  def to_rna(self):
    return self.replace("T", "U")

  def proteins(self):
    def find_proteins(string):
      proteins = []
      for i in range(len(string) - 2):
        codon = str(string[i: i + 3])
        if codon in Seq.start_codons:
          frame = string[i:]
          protein_or_none = Seq(frame).to_protein()
          if protein_or_none:
            proteins.append(protein_or_none)
      return proteins

    rna = self.to_rna()
    rna_reversed = Seq(self.reverse_complement()).to_rna()
    return set(find_proteins(rna) + find_proteins(rna_reversed))

  def reverse_complement(self):
    return (self
              .replace("T", "a")
              .replace("C", "g")
              .replace("G", "c")
              .replace("A", "t")
              .upper()[::-1])

  def __generate_tuples(self, k=3):
    for i in range(0, int(len(self) / k)):
      yield self[i * k:(i * k) + k]

strings = parse_file("dataset")
seq = Seq(strings[0])
for protein in seq.proteins():
  print(protein)