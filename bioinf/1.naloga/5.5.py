import numpy as np

production = True
delim = "\t" if production else " "

class Data:
  def __init__(self, file):
    self.emitted, self.alphabet, self.path, self.states = self._parse_file(file)

  def _parse_file(self, file):
    with open(file) as handle:
      emitted = handle.readline()[:-1]
      _ = handle.readline()
      alphabet = list(handle.readline().replace(" ", "").replace(delim, "")[:-1])
      _ = handle.readline()
      path = handle.readline()[:-1]
      _ = handle.readline()
      states = list(handle.readline().replace(" ", "").replace(delim, "")[:-1])
      return emitted, alphabet, path, states + ["C"]

  def compute(self):
    A = np.full((len(self.states), len(self.states)), 0)
    B = np.full((len(self.states), len(self.alphabet)), 0)

    for i, letter in enumerate(self.emitted):
      idx = self.states.index(self.path[i])
      idx2 = self.alphabet.index(letter)
      B[idx, idx2] += 1

    for i, state in enumerate(self.path[1:]):
      idx = self.states.index(self.path[i])
      idx2 = self.states.index(self.path[i + 1])
      A[idx, idx2] += 1

    B = B / B.sum(axis=1).reshape(-1, 1)
    A = A / A.sum(axis=1).reshape(-1, 1)
    return A, B

data = Data("dataset")
B, A = data.compute()
for state in data.states:
  print(state + "\t", end="")
print()
for state, row in zip(data.states, B):
  print(str(state) + "\t", end="")
  for val in row:
    print("%.3f" % val + ("" if val == row[-1] else "\t"), end="")
  print()
print("--------")
print("\t", end="")
for a in data.alphabet:
  print(a + "\t", end="")
print()
for state, row in zip(data.states, A):
  print(str(state) + "\t", end="")
  for val in row:
    print("%.3f" % val + ("" if val == row[-1] else "\t"), end="")
  print()