import numpy as np

def p_distance(s1, s2):
  assert len(s1) == len(s2)
  return 1 - sum(a == b
                 for a, b in zip(s1, s2)) / len(s1)

class DistanceMatrix:
  def __init__(self, data):
    self.data = data
    self.matrix = self.__generate()

  def __generate(self):
    matrix = np.zeros(shape=(len(self.data), len(self.data)))
    for i, s1 in enumerate(self.data):
      for j, s2 in enumerate(self.data):
        distance = 0 if i == j else p_distance(s1, s2)
        matrix[i][j] = distance
    return matrix

  def __str__(self):
    s = ""
    for col in self.matrix:
      for j, row in enumerate(col):
        s += "%.5f" % row
        s += "" if j + 1 == len(col) else " "
      s += "\n"
    return s


def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

print(DistanceMatrix(parse_file("dataset")))