def parse_file(file):
  with open(file) as handle:
    return [line.strip() for line in handle.readlines()]

dataset = parse_file("dataset")

class DeBruijnGraph:
  def __init__(self, dnas):
    self.dna_set = set(dnas)
    self.edges = {dna[:-1]: dna[1:] for dna in dnas}

  def cyclic_superstring(self):
    cyclic_superstring = ""
    current = list(self.edges.items())[0][0]

    for _ in range(len(self.edges)):
      cyclic_superstring += self.edges[current][-1]
      current = self.edges[current]

    return cyclic_superstring


graph = DeBruijnGraph(dataset)
print(graph.cyclic_superstring())