import numpy as np


def neighbor_joining(d, nodes, n, inner_node):

    if n > 2:
        # D' ← neighbor-joining matrix constructed from the distance matrix D
        d_prime = np.array([[0] * n for _ in range(n)])
        for i in range(n):
            for j in range(i+1, n):
                d_prime[i][j] = (n - 2) * d[i, j] - sum(d[i, :]) - sum(d[j, :])
                d_prime[j][i] = d_prime[i][j]

        # find elements i and j such that D'i,j is a minimum non-diagonal element of D'
        i, j = np.unravel_index(d_prime.argmin(), d_prime.shape)
        # print(i, j)

        # Δ ← (TotalDistanceD(i) - TotalDistanceD(j)) / (n - 2)
        delta = (sum(d[i, :]) - sum(d[j, :])) / (n - 2)

        # set limbLength(i) ← (1 / 2)(Di,j + Δ)
        limb_length_i = 0.5 * (d[i, j] + delta)
        # print('limb_length_i', limb_length_i)

        # set limbLength(j) ← (1 / 2)(Di,j - Δ)
        limb_length_j = 0.5 * (d[i, j] - delta)
        # print('limb_length_j', limb_length_j)

        # add a new row/column m to D so that Dk,m = Dm,k = (1/2)(Dk,i + Dk,j - Di,j) for any k
        m = [0] + [0.5 * (d[k][i] + d[k][j] - d[i][j]) for k in range(n) if k != i and k != j]
        # print(m)

        d_temp = np.zeros((n-1, n-1))
        # remove rows i and j from D
        d = np.delete(d, (i, j), axis=0)
        # remove columns i and j from D
        d = np.delete(d, (i, j), axis=1)
        d_temp[1:, 1:] = d
        d_temp[0, :], d_temp[:, 0] = m, m

        item_i = nodes[i]
        item_j = nodes[j]
        nodes.remove(item_i)
        nodes.remove(item_j)
        nodes.insert(0, inner_node)

        graph = neighbor_joining(d_temp, nodes, n - 1, inner_node + 1)
        # link item_i and inner node
        graph.append((item_i, (inner_node, limb_length_i)))
        graph.append((inner_node, (item_i, limb_length_i)))
        # link item_j and inner node
        graph.append((item_j, (inner_node, limb_length_j)))
        graph.append((inner_node, (item_j, limb_length_j)))
        return graph

    else:
        node1, node2 = nodes
        graph = [(node1, (node2, d[0, 1])), (node2, (node1, d[1, 0]))]
        return graph


if __name__ == '__main__':
    with open('dataset', 'r') as f:
        size = int(f.readline())
        node_list = list(range(size))
        dist_matrix = [[int(n) for n in line.strip().split("  ")]
                       for line in f.readlines()]
        dist_matrix = np.array(dist_matrix)

        T = neighbor_joining(dist_matrix, node_list, size, size)

        # pretty print
        for link in (sorted(T, key=lambda x: x[0])):
            print("{}->{}:{:.2f}".format(link[0], link[1][0], round(link[1][1], 2)))
