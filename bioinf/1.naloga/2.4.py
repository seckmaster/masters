alphabet = list("ABCDEFG")
n = 3

def generate_combinations(n, alphabet):
  if n == 1:
    for letter in alphabet:
      yield letter
  else:
    for letter in alphabet:
      for x in generate_combinations(n - 1, alphabet):
        yield "" + letter + x


for combination in generate_combinations(n, alphabet):
  print(combination, end=" ")