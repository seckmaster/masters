class Data:
  def __init__(self, file):
    self.hidden, self.alphabet, self.string, self.states, self.probabilities = self._parse_file(file)

  def _parse_file(self, file):
    with open(file) as handle:
      hidden = handle.readline()[:-1]
      _ = handle.readline()
      alphabet = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])
      _ = handle.readline()
      string = handle.readline()[:-1]
      _ = handle.readline()
      states = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])
      _ = handle.readline()
      _ = handle.readline()

      probabilities = [[float(val)
                      for val in line.strip().split("\t")[1:] if len(val) > 0]
                      for line in handle.readlines()]
      return hidden, alphabet, string, states, probabilities

  def prob(self, a, b):
    index_a = self.states.index(a)
    index_b = self.alphabet.index(b)
    return self.probabilities[index_a][index_b]

  def compute(self):
    initial = 1
    from functools import reduce
    return reduce(lambda acc, pair: acc * (self.prob(pair[0], pair[1])),
                  zip(self.string, self.hidden),
                  initial)

data = Data("dataset")
print(data.compute())