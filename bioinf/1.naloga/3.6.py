import numpy as np

def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

strings = parse_file("dataset")

class ConsensusMatrix:
  def __init__(self, data):
    def validate(data):
      return len(set(len(i) for i in data)) == 1

    assert validate(data)
    self.alphabet = "ACGT"
    self.data = data
    self.matrix = self.__generate_matrix()

  def __generate_matrix(self):
    def count_letter(letter, index):
      return sum(s[index] == letter for s in self.data)

    matrix = np.zeros(shape=(len(self.alphabet),
                             len(self.data[0])))
    for i in range(len(self.data[0])):
      matrix[0][i] = count_letter(self.alphabet[0], i)
      matrix[1][i] = count_letter(self.alphabet[1], i)
      matrix[2][i] = count_letter(self.alphabet[2], i)
      matrix[3][i] = count_letter(self.alphabet[3], i)
    return matrix

  def consensus_string(self):
    s = ""
    for col in self.matrix.T:
      s += self.alphabet[np.argmax(col, axis=0)]
    return s

  def __str__(self):
    s = ""
    for i, col in enumerate(self.matrix):
      s += self.alphabet[i] + ": "
      for j, row in enumerate(col):
        s += "%d" % row
        s += "" if j + 1 == len(col) else " "
      s += "\n"
    return self.consensus_string() + "\n" + s


matrix = ConsensusMatrix(parse_file("dataset"))
print(matrix)