import numpy as np

def parse_file(file):
  with open(file) as handle:
    _ = int(handle.readline())
    mat = [[int(n) for n in line.strip().split("\t")]
           for line in handle.readlines()]
    return np.array(mat)

def n_joining(matrix):
  def find_minimal(matrix):
    size = len(matrix)
    d = [[0] * size for _ in range(size)]
    for i in range(size):
      for j in range(i + 1, len(matrix)):
        val = (size - 2) * matrix[i, j] - sum(matrix[i, :]) - sum(matrix[j, :])
        d[i][j] = val
        d[j][i] = val

    i, j = None, None
    for a in range(size):
      for b in range(size):
        if a == b or a + b == size:
          continue
        if i is None or d[i][j] > d[a][b]:
          i, j = a, b
    return i, j

  def algorithm(matrix, nodes, node):
    size = len(matrix)

    if size == 2:
      graph = [(nodes[0], (nodes[1], matrix[0, 1])),
               (nodes[1], (nodes[0], matrix[1, 0]))]
      return graph

    d = np.zeros(shape=(size - 1, size - 1))
    i, j = find_minimal(matrix)
    ith = nodes[i]
    jth = nodes[j]

    nodes.remove(ith)
    nodes.remove(jth)
    nodes.insert(0, node)

    delta = (sum(matrix[i, :]) - sum(matrix[j, :])) / (size - 2)
    limb_length_i = 0.5 * (matrix[i, j] + delta)
    limb_length_j = 0.5 * (matrix[i, j] - delta)

    m = [0.5 * (matrix[k][i] + matrix[k][j] - matrix[i][j])
         for k in range(size) if k != i and k != j]

    matrix = np.delete(matrix, (i, j), axis=0)
    matrix = np.delete(matrix, (i, j), axis=1)
    d[0:, ] = [0] + m
    d[:, 0] = [0] + m
    d[1:, 1:] = matrix
    T = algorithm(d, nodes, node + 1)

    # print(i, node, limb_length_i)
    # print(j, node, limb_length_j)
    # print()
    T.append((ith, (node, limb_length_i)))
    T.append((node, (ith, limb_length_i)))
    T.append((jth, (node, limb_length_j)))
    T.append((node, (jth, limb_length_j)))
    return T

  matrix = matrix[:]
  res = algorithm(matrix,
                  nodes=[i for i in range(len(matrix))],
                  node=len(matrix))
  return sorted(res, key=lambda x: x[0])

def print_tree(tree):
  for (a, (b, c)) in tree:
    print("{}->{}:{:.2f}".format(a, b, c))

matrix = parse_file("dataset")
tree = n_joining(matrix)
print_tree(tree)