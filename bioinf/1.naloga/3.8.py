codon_table = {
"UUU": "F",      "CUU": "L",      "AUU": "I",     "GUU": "V",
"UUC": "F",      "CUC": "L",      "AUC": "I",     "GUC": "V",
"UUA": "L",      "CUA": "L",      "AUA": "I",     "GUA": "V",
"UUG": "L",      "CUG": "L",      "AUG": "M",     "GUG": "V",
"UCU": "S",      "CCU": "P",      "ACU": "T",     "GCU": "A",
"UCC": "S",      "CCC": "P",      "ACC": "T",     "GCC": "A",
"UCA": "S",      "CCA": "P",      "ACA": "T",     "GCA": "A",
"UCG": "S",      "CCG": "P",      "ACG": "T",     "GCG": "A",
"UAU": "Y",      "CAU": "H",      "AAU": "N",     "GAU": "D",
"UAC": "Y",      "CAC": "H",      "AAC": "N",     "GAC": "D",
"UAA": "Stop",   "CAA": "Q",      "AAA": "K",     "GAA": "E",
"UAG": "Stop",   "CAG": "Q",      "AAG": "K",     "GAG": "E",
"UGU": "C",      "CGU": "R",      "AGU": "S",     "GGU": "G",
"UGC": "C",      "CGC": "R",      "AGC": "S",     "GGC": "G",
"UGA": "Stop",   "CGA": "R",      "AGA": "R",     "GGA": "G",
"UGG": "W",      "CGG": "R",      "AGG": "R",     "GGG": "G",
}

def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

def dna_to_rna(string):
  return string.replace("T", "U")

def generate_tuples(string, k=3):
  for i in range(0, int(len(string) / k)):
    yield string[i * k:(i*k) + k]

strings = parse_file("dataset")
dna, intros = strings[0], strings[1:]
for intro in intros:
  dna = dna.replace(intro, "")

rna = dna_to_rna(dna)
s = ""
for tuple in generate_tuples(rna):
  if codon_table[tuple] == "Stop": break
  s += codon_table[tuple]
print(s)