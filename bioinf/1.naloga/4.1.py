import numpy as np

def edit_distance(t, s):
  # create edit matrix
  edit_matrix = np.full((len(s), len(t)), 0)

  # fill initial values
  edit_matrix[0, :] = np.array([i for i in range(len(t))])
  edit_matrix[:, 0] = np.array([i for i in range(len(s))])

  for i in range(1, len(s)):
    for j in range(1, len(t)):

      if s[i - 1] == t[j - 1]:
        edit_matrix[i][j] = edit_matrix[i - 1][j - 1]
      else:
        edit_matrix[i][j] = min((edit_matrix[i - 1][j] + 1,
                                 edit_matrix[i][j - 1] + 1,
                                 edit_matrix[i - 1][j - 1] + 1))

  return edit_matrix[-1][-1]

def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

data = parse_file("dataset")
print(edit_distance(data[0], data[1]))