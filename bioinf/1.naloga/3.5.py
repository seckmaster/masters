def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

def transition_traversion_ratio(s1, s2):
  def is_transition(c1, c2, transition_set={"AG", "GA", "CT", "TC"}):
    return c1+c2 in transition_set
  def is_transversion(c1, c2):
    return c1 != c2 and not is_transition(c1, c2)

  assert len(s1) == len(s2)
  return sum(is_transition(c1, c2) for c1, c2 in zip(s1, s2)) / sum(is_transversion(c1, c2) for c1, c2 in zip(s1, s2))

s1, s2 = parse_file("dataset")
print(transition_traversion_ratio(s1, s2))