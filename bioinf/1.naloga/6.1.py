def parse_file(file):
  with open(file) as handle:
    return [line.strip() for line in handle.readlines()]

dataset = parse_file("dataset")

class DeBruijnGraph:
  def __init__(self, dnas):
    self.dna_set = set(dnas).union(set(self.__reverse_complement(dna) for dna in dnas))
    self.vertices = []
    for dna in self.dna_set:
      for mer in self.generate_k_terms(dna, k=len(dna) - 1):
        self.vertices.append(mer)
    self.edges = self.construct_graph()

  def __reverse_complement(self, string):
    return string\
      .replace("T", "a")\
      .replace("C", "g")\
      .replace("G", "c")\
      .replace("A", "t")\
      .upper()[::-1]

  def generate_k_terms(self, string, k=3):
    for i in range(-1, int(len(string) - (k))):
      yield string[len(string) - (i + k) - 1: len(string) - i - 1]

  def construct_graph(self):
    edges = []
    for dna in self.dna_set:
      edge = dna[0: len(dna) - 1], dna[1: len(dna)]
      edges.append(edge)
    return edges

  def cyclic_superstring(self):
    cyclic_superstring = []
    current_read_prefix = list(self.dna_set)[0]

    while len(self.dna_set):
      cyclic_superstring.append(self.dna_set[current_read_prefix][-1])
      current_read_prefix = self.dna_set.pop(current_read_prefix, None)

    return cyclic_superstring


graph = DeBruijnGraph(dataset)
print(graph.cyclic_superstring())