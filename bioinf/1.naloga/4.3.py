def generate_k_terms(string, k):
  for i in range(-1, int(len(string) - (k))):
    yield string[len(string) - (i + k) - 1: len(string) - i - 1]

def generate_substrings(string):
  yield string
  for j in range(len(string)):
    k = len(string) - j
    for tuple in generate_k_terms(string, k):
      yield tuple

def lcs(strings):
  def in_all(other_strings, suffix):
    return sum(suffix in string
               for string in other_strings) == len(other_strings)

  for suffix in generate_substrings(strings[0]):
    if in_all(strings[1:], suffix):
      return suffix

def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

print(lcs(parse_file("dataset")))
