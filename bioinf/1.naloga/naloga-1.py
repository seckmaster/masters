import numpy as np


file_name = "naloga-1.txt"

def read_matrix(file_name) :
    file = open( file_name, 'r')
    ln = int( file.readline())
    lines = file.read()
    matrix = np.array([ np.array([int(n) for n in line.split('\t') if n != ""]) for line in lines.split('\n')])

    if matrix.shape != (ln, ln) :
        raise Exception("Wrong matrix shape!")
    return matrix, ln

def Q_calc(matrix, i1, i2) :
    ln = len( matrix)
    return (ln - 2) * matrix[i1, i2] - matrix[i1, :].sum() - matrix[i2, :].sum()

def neighbor_joining (matrix, selection, id) :
    n = len( matrix)
    if n == 2 :
        return [(selection[0], selection[1], matrix[0, 1]),
                (selection[0], selection[1], matrix[1, 0])]

    distance = np.array([ np.array([ 0 if i1 == i2 else Q_calc(matrix, i1, i2) for i2 in range(n)]) for i1 in range(n)])
    i, j = np.unravel_index( distance.argmin(), distance.shape)
    id_i, id_j = selection[ i], selection[ j]

    delta = (matrix[i, :].sum() - matrix[j, :].sum()) / (n-2)

    limbLenghtI = 1/2 * (matrix[i, j] + delta)
    limbLenghtJ = 1/2 * (matrix[i, j] - delta)

    distane_row = [1/2 * (matrix[k, i] + matrix[k, j] - matrix[i, j]) for k in range(n)
                            if k != i and k != j]

    matrix = np.delete(matrix, [i, j], axis=0)
    matrix = np.delete(matrix, [i, j], axis=1)
    selection.remove( id_i)
    selection.remove( id_j)
    selection.insert(0, id)

    new_matrix = np.zeros((n-1, n-1))
    new_matrix[0:, ] = [0] + distane_row
    new_matrix[:, 0] = [0] + distane_row
    new_matrix[1:, 1:] = [1] * matrix

    # print( selection)

    next_step = neighbor_joining( new_matrix, selection, id+1)

    next_step.extend([
        (id_i, id, limbLenghtI),
        (id, id_i, limbLenghtI),
        (id_j, id, limbLenghtJ),
        (id, id_j, limbLenghtJ),
    ])

    return next_step


distance_matrix, lines = read_matrix( file_name)
selection = list( np.arange(0, lines))
tree = neighbor_joining( distance_matrix, selection, lines)

tree = sorted( tree, key=lambda x: x[0])

exit = open('naloga1-out.txt', "w")
exit.write("\n".join( "{}->{}:{:.2f}".format(start, end, val) for start, end, val in tree))