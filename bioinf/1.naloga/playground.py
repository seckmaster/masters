from collections import defaultdict

def gc_count(string):
  gc_content = len([i for i in string if i == "G" or i == "C"])
  return gc_content / len(string)

def parse_file(file):
  f = open(file)
  dna_samples = defaultdict(str)
  for line in f.readlines():
    if line[0] == ">":
      name = line[1:-1]
      continue
    dna_samples[name] += line[:-1] if line[len(line) - 1] == "\n" else line
  f.close()
  return dna_samples

samples = parse_file("dataset")
gc = dict()

items = ((gc_count(dna), name) for name, dna in samples.items())
max_gc = max(items, key=lambda x: x[0])
print(max_gc[1])
print(max_gc[0] * 100)