import numpy as np

def edit_distance(s, t):
  len_s = len(s) + 1
  len_t = len(t) + 1

  # create edit matrix
  edit_matrix = np.full((len(s) + 1, len(t) + 1), 0)

  # fill initial values
  edit_matrix[0, :] = np.array([i for i in range(len_t)])
  edit_matrix[:, 0] = np.array([i for i in range(len_s)])

  for i in range(1, len_s):
    for j in range(1, len_t):
      edit_matrix[i][j] = min([edit_matrix[i - 1][j] + 1,
                               edit_matrix[i][j - 1] + 1,
                               edit_matrix[i - 1][j - 1] + (s[i - 1] != t[j - 1])])

  alignment_s = ''
  alignment_t = ''
  i, j = len(s), len(t)

  while i > 0 or j > 0:
    if i > 0 and j > 0 and edit_matrix[i][j] == edit_matrix[i - 1][j - 1] + (s[i - 1] != t[j - 1]):
      alignment_s = s[i - 1] + alignment_s
      alignment_t = t[j - 1] + alignment_t
      i -= 1
      j -= 1
    elif i > 0 and edit_matrix[i][j] == edit_matrix[i - 1][j] + 1:
      alignment_s = s[i - 1] + alignment_s
      alignment_t = '-' + alignment_t
      i -= 1
    else:
      alignment_s = '-' + alignment_s
      alignment_t = t[j - 1] + alignment_t
      j -= 1

  return edit_matrix[-1][-1], alignment_s, alignment_t

def hamming_distance(s1, s2):
  """Return the Hamming distance between equal-length sequences"""
  return sum(el1 != el2 for el1, el2 in zip(s1, s2)) + abs(len(s1) - len(s2))

def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

data = parse_file("dataset")
a, b, c = edit_distance(data[0], data[1])
print(a)
print(b)
print(c)