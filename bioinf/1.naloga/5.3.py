import numpy as np

class Data:
  def __init__(self, file):
    self.hidden, self.alphabet, self.states, self.transition_matrix, self.emission_matrix = self._parse_file(file)

  def _parse_file(self, file):
    with open(file) as handle:
      hidden = handle.readline()[:-1]
      _ = handle.readline()
      alphabet = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])
      _ = handle.readline()
      states = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])
      _ = handle.readline()
      _ = handle.readline()

      transition_matrix = [[float(val)
                            for val in handle.readline().strip().split("\t")[1:] if len(val) > 0]
                          for _ in range(len(states))]
      _ = handle.readline()
      _ = handle.readline()
      emission_matrix = [[float(val)
                          for val in line.strip().split("\t")[1:] if len(val) > 0]
                         for line in handle.readlines()]
      return hidden, alphabet, states, np.array(transition_matrix), np.array(emission_matrix)

  def start_prob(self):
    return 0.5

class Viterbi:
  def __init__(self, data):
    self.data = data

  def compute(self):
    # https://en.wikipedia.org/wiki/Viterbi_algorithm
    O = self.data.hidden
    A = self.data.transition_matrix
    B = self.data.emission_matrix
    Y = self.data.alphabet
    S = self.data.states
    T1 = np.full((len(S), len(O)), np.nan)
    T2 = np.full((len(S), len(O)), np.nan)

    for i, state in enumerate(S):
      T1[i, 0] = 0.5 * B[i, Y.index(O[0])]
      T2[i, 0] = 0

    for i, letter in enumerate(O[1:]):
      for j, state in enumerate(S):
        T1[j, i + 1] = B[j, Y.index(letter)] * max(T1[k, i] * A[k, S.index(state)]
                                                   for k in range(len(S)))
        T2[j, i + 1] = np.argmax([T1[k, i] * A[k, S.index(state)]
                                  for k in range(len(S))])

    zt = int(np.argmax(T1[:, -1]))
    result = S[zt]

    for i in range(len(O) - 1):
      idx = len(O) - i - 1
      result = S[int(T2[zt, idx])] + result
      if int(T2[zt, idx]) != zt:
        zt = int(T2[zt, idx])

    return result

data = Data("dataset")
print(Viterbi(data).compute())