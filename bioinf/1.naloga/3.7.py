def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

def reverse_complement(string):
  return string\
    .replace("T", "a")\
    .replace("C", "g")\
    .replace("G", "c")\
    .replace("A", "t")\
    .upper()[::-1]

def is_reverse_palindrom(s):
  return len(s) > 2 and s == reverse_complement(s)

def reverse_palindroms(s, rng=range(4,13)):
  for i in range(len(s)):
    for j in rng:
      if i + j > len(s): break
      substr = s[i: i+j]
      if is_reverse_palindrom(substr):
        print(i + 1, len(substr))

string = parse_file("dataset")[0]
reverse_palindroms(string)