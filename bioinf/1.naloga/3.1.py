from math import log10
from functools import reduce

dataset = "CCGCCTCTGCATGGCGATCACGTGTTAGTGCGATAGACTGTCTGCAGCCCTAGGGGTAGAGGCACGAGGTTTGTGCACAGATACCC"
A = "0.071 0.162 0.177 0.258 0.316 0.387 0.418 0.470 0.530 0.598 0.661 0.698 0.751 0.786 0.859 0.942"
A = [float(i) for i in A.split(" ")]

def string_equal_probability(gc_prob, string):
  g_c_prob = gc_prob * 0.5
  a_t_prob = (1 - gc_prob) * 0.5
  gc = string.count("G") + string.count("C")
  at = len(string) - gc
  return g_c_prob**gc * a_t_prob ** at

def blow_up_function(x):
  return log10(x)

res = [string_equal_probability(prob, dataset)
       for prob in A]
res = [blow_up_function(x) for x in res]
for r in res:
  print("%.3f" % r, end=" ")