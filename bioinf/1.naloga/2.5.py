dataset = "CAGCATGGTATCACAGCAGAG"

# def generate_prefixes(string, n):
#   for j in range(n):
#     yield string[0: j]
#

# def generate_failure_array(dataset):
#   for prefix in stream:
#     k = len(prefix)
#     failure = 0
#     for j in range(1, k):
#       substring = prefix[j:]
#       subprefix = prefix[0: len(substring)]
#       if subprefix == substring and failure < len(substring):
#         failure = len(substring)
#     yield failure
#
# for p in list(generate_failure_array(stream_file("dataset"))):
#   print(p, end=" ")

def parse_file(file):
  with open(file) as handle:
    return "".join(line[:-1] for line in handle if line[0] != ">")

def write_result(result, to_file):
  with open(to_file, "w") as handle:
    result_string = " ".join(str(el) for el in result)
    handle.write(result_string)

def kmp_failure_array(w):
  # https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm#%22Partial_match%22_table_(also_known_as_%22failure_function%22)
  failure_array = [None]

  for i in range(0, len(w)):
    j = i

    while True:
      if j == 0:
        failure_array.append(0)
        break

      if w[failure_array[j]] == w[i]:
        failure_array.append(failure_array[j] + 1)
        break

      j = failure_array[j]

  return failure_array[1:]

write_result(kmp_failure_array(parse_file("dataset")), "output")

