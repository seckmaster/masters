import numpy as np

class InvalidPairException(Exception):
  pass

class Matrix:
  def __init__(self, matrix_filename):
    self._load_matrix(matrix_filename)

  def _load_matrix(self, matrix_filename):
    with open(matrix_filename) as matrix_file:
      matrix = matrix_file.read()
    lines = matrix.strip().split('\n')

    header = lines.pop(0)
    columns = header.split()
    matrix = {}

    for row in lines:
      entries = row.split()
      row_name = entries.pop(0)
      matrix[row_name] = {}

      if len(entries) != len(columns):
        raise Exception('Improper entry number in row')
      for column_name in columns:
        matrix[row_name][column_name] = entries.pop(0)

    self._matrix = matrix

  def lookup_score(self, a, b):
    a = a.upper()
    b = b.upper()

    if a not in self._matrix or b not in self._matrix[a]:
      raise InvalidPairException('[%s, %s]' % (a, b))
    return int(self._matrix[a][b])

def edit_distance(s, t, matrix, d):
  len_s = len(s) + 1
  len_t = len(t) + 1

  # create edit matrix
  edit_matrix = np.full((len(s) + 1, len(t) + 1), 0)

  # fill initial values
  edit_matrix[0, :] = np.array([i * -d for i in range(len_t)])
  edit_matrix[:, 0] = np.array([i * -d for i in range(len_s)])

  for i in range(1, len_s):
    for j in range(1, len_t):
      score = matrix.lookup_score(s[i - 1], t[j - 1])
      edit_matrix[i][j] = max([edit_matrix[i - 1][j] - d,
                               edit_matrix[i][j - 1] - d,
                               edit_matrix[i - 1][j - 1] + score])

  return edit_matrix[-1][-1]

def parse_file(file):
  with open(file) as handle:
    strings = []
    string = ""
    for line in handle.readlines():
      if line.startswith(">"):
        if string:
          strings.append(string)
        string = ""
        continue
      string += line.strip()
    strings.append(string)
    return strings

matrix = Matrix("blosum62.txt")
data = parse_file("dataset")
print(edit_distance(data[0], data[1], matrix, d=5))