class Data:
  def __init__(self, file):
    self.string, self.states, self.probabilities = self._parse_file(file)

  def _parse_file(self, file):
    with open(file) as handle:
      string = handle.readline()[:-1]
      for _ in range(3):
        _ = handle.readline()
      states = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])

      probabilities = [[float(val)
                      for val in line.strip().split("\t")[1:] if len(val) > 0]
                      for line in handle.readlines()]
      return string, states, probabilities

  def prob(self, a, b):
    index_a = self.states.index(a)
    index_b = self.states.index(b)
    return self.probabilities[index_a][index_b]

  def compute(self):
    initial = 0.5
    from functools import reduce
    return reduce(lambda acc, pair: acc * (self.prob(pair[0], pair[1])),
                  zip(self.string, self.string[1:]),
                  initial)

data = Data("dataset")
print(data.compute())