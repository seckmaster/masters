import numpy as np

production = True
delim = "\t" if production else " "

class Data:
  def __init__(self, file):
    self.hidden, self.alphabet, self.states, self.transition_matrix, self.emission_matrix = self._parse_file(file)

  def _parse_file(self, file):
    with open(file) as handle:
      _ = handle.readline()
      _ = handle.readline()
      hidden = handle.readline()[:-1]
      _ = handle.readline()
      alphabet = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])
      _ = handle.readline()
      states = list(handle.readline().replace(" ", "").replace("\t", "")[:-1])
      _ = handle.readline()
      _ = handle.readline()

      transition_matrix = [[float(val)
                            for val in handle.readline().strip().split(delim)[1:] if len(val) > 0]
                          for _ in range(len(states))]
      _ = handle.readline()
      _ = handle.readline()
      emission_matrix = [[float(val)
                          for val in line.strip().split(delim)[1:] if len(val) > 0]
                         for line in handle.readlines()]
      return hidden, alphabet, states, np.array(transition_matrix), np.array(emission_matrix)

class Viterbi:
  def __init__(self, data):
    self.data = data

  def compute(self, emission=None, transition=None):
    # https://en.wikipedia.org/wiki/Viterbi_algorithm
    O = self.data.hidden
    A = transition if transition is not None else self.data.transition_matrix
    B = emission if emission is not None else self.data.emission_matrix
    Y = self.data.alphabet
    S = self.data.states
    T1 = np.full((len(S), len(O)), np.nan)
    T2 = np.full((len(S), len(O)), np.nan)

    for i, state in enumerate(S):
      T1[i, 0] = 0.5 * B[i, Y.index(O[0])]
      T2[i, 0] = 0

    for i, letter in enumerate(O[1:]):
      for j, state in enumerate(S):
        T1[j, i + 1] = B[j, Y.index(letter)] * max(T1[k, i] * A[k, S.index(state)]
                                                   for k in range(len(S)))
        T2[j, i + 1] = np.argmax([T1[k, i] * A[k, S.index(state)]
                                  for k in range(len(S))])

    zt = int(np.argmax(T1[:, -1]))
    result = S[zt]

    for i in range(len(O) - 1):
      idx = len(O) - i - 1
      result = S[int(T2[zt, idx])] + result
      if int(T2[zt, idx]) != zt:
        zt = int(T2[zt, idx])

    return result

  def compute_probability(self):
    # https://en.wikipedia.org/wiki/Viterbi_algorithm
    O = self.data.hidden
    A = self.data.transition_matrix
    B = self.data.emission_matrix
    Y = self.data.alphabet
    S = self.data.states
    T1 = np.full((len(S), len(O)), np.nan)

    for i, state in enumerate(S):
      T1[i, 0] = 0.5 * B[i, Y.index(O[0])]

    for i, letter in enumerate(O[1:]):
      for j, state in enumerate(S):
        T1[j, i + 1] = B[j, Y.index(letter)] * sum(T1[k, i] * A[k, S.index(state)]
                                                   for k in range(len(S)))

    return sum(T1[:, -1])

  def compute2(self, path):
    A = np.full((len(self.data.states), len(self.data.states)), 0)
    B = np.full((len(self.data.states), len(self.data.alphabet)), 0)

    for i, letter in enumerate(self.data.hidden):
      idx = self.data.states.index(path[i])
      idx2 = self.data.alphabet.index(letter)
      B[idx, idx2] += 1

    for i, state in enumerate(path[1:]):
      idx = self.data.states.index(path[i])
      idx2 = self.data.states.index(path[i + 1])
      A[idx, idx2] += 1

    B = B / B.sum(axis=1).reshape(-1, 1)
    A = A / A.sum(axis=1).reshape(-1, 1)
    return A, B

data = Data("dataset")
B, A = data.emission_matrix, data.transition_matrix
for _ in range(100):
  path = Viterbi(data).compute(emission=B, transition=A)
  A, B = Viterbi(data).compute2(path)

for state in data.states:
  print(state + "\t", end="")
print()
for state, row in zip(data.states, B):
  print(str(state) + "\t", end="")
  for val in row:
    print("%.3f" % val + ("" if val == row[-1] else "\t"), end="")
  print()
print("--------")
print("\t", end="")
for a in data.alphabet:
  print(a + "\t", end="")
print()
for state, row in zip(data.states, A):
  print(str(state) + "\t", end="")
  for val in row:
    print("%.3f" % val + ("" if val == row[-1] else "\t"), end="")
  print()